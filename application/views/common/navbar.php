<nav class="navbar navbar-expand-lg navbar-dark primary-color">

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="basicExampleNav">

    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<?= base_url('inicio/index')?>">Inicio
          <span class="sr-only">(current)</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('inicio/api')?>">API</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('inicio/funcionamento')?>">Funcionamento</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('inicio/interacao')?>">Interação</a>
      </li>

    </ul>

   
  </div>

</nav>
