<div class="container" style=" margin-top: 5%; margin-bottom: 5%; box-shadow: 1px 1px 1px 1px; padding: 2%;">
    <span style="font-size: 22px; font-weight: bold;">O que é API?</span>
    <p style="font-size: 15px; text-align: justify;">A sigla API corresponde às palavras em inglês “Application Programming Interface“. No português “Interface de 
    Programação de Aplicações”. Elas são uma forma de integrar sistemas, possibilitando benefícios como a segurança dos dados,
     facilidade no intercâmbio entre informações com diferentes linguagens de programação e a monetização de acessos.  </p>
     <p style="font-size: 15px; text-align: justify;">As APIs são um tipo de “ponte” que conectam aplicações, podendo ser utilizadas para os mais variados tipos de negócio, por empresas de diversos nichos de mercado 
     ou tamanho, conforme veremos melhor ao longo de nosso post.</p>

     <img class="card-img-top" src="<?= base_url('assets/img/api.png')?>" alt="api">

     <p style="font-size: 15px; text-align: justify;">A maioria das pessoas não sabe o que é uma API. Isso porque elas são invisíveis ao usuário comum, que enxerga 
     apenas a interface dos softwares e aplicativos. No entanto, os profissionais de programação conhecem por dentro essa tecnologia que é resultado da evolução de
      diversos sistemas e ferramentas. Aplicativos e softwares de diversos tipos são apenas passíveis de construção por meio dos padrões e especificações disponibilizados
       pelas APIs. </p>

       <p style="font-size: 15px; text-align: justify;"> As APIs proporcionam a integração entre sistemas que possuem linguagem totalmente distintas de maneira ágil e segura. Em outras formas de integração de sistemas, o profissional que realiza o trabalho precisa, muitas vezes, instalar recursos compatíveis com o sistema no qual se busca efetuar a integração, gerando um grande trabalho e, 
    consequentemente, atraso na geração de negócios e processos produtivos de uma companhia. </p>
       
    </div>
