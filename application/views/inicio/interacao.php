<div class="container" style="margin-top: 5%; margin-bottom: 5%; box-shadow: 1px 1px 1px 1px; padding: 2%;">
    <span style="font-size: 22px; font-weight: bold;">Relatório de interação entre usuário e API</span>
    <p style="font-size: 15px; text-align: justify;">Para uso da API do Data2CRM, é necessário que ela seja implantada no sistema desejado. Para o projeto atual, foi 
    utilazado o PHP com CodeIgniter. </p>
     <p style="font-size: 15px; text-align: justify;">Os arquivos da API estão localizados na pasta "application/api", em com todos os métodos e interações possíveis
     que a API fornece ao usuário.</p>

     <img class="card-img-top" src="<?= base_url('assets/img/relatorio.jpg')?>" alt="relatorio">

     <p style="font-size: 15px; text-align: justify;">Para a migração de dados ser realizada, deve ser selecionado todos os dados que o usuário deseja migrar e o local 
     destino para esse conteúdo. A integração torna-se viavél pois, ao implantada num sistema, o usuário poderá migrar todos os dados desejados
       com extrema facilidade.</p>

       <p style="font-size: 15px; text-align: justify;">Para utilização da API, é necessário solicitar ao fornecedor uma demonstração gratuíta da mesma. Não existem tutorias de
       uso em PHP bem explicados na internet.</p>
       
    </div>