<div class="container" style="margin-top: 5%; margin-bottom: 5%; box-shadow: 1px 1px 1px 1px; padding: 2%;">
    <span style="font-size: 22px; font-weight: bold;">Como funciona a Data2CRM?</span>
    <p style="font-size: 15px; text-align: justify;">A Data2CRM nada mais é do que um serviço de migração de datas, que tem como intuito importar e exportar as informações 
    desejadas pelo úsuario para outros locais.  </p>
     <p style="font-size: 15px; text-align: justify;">É possível realizar esse serviço com diversos tipos de dados, como contas, contatos, oportunidades, dicas, usuários, notas, produtos
    e etc. </p>

     <img class="card-img-top" src="<?= base_url('assets/img/migration.jpg')?>" alt="migration">

     <p style="font-size: 15px; text-align: justify;">No caso da Data2CRM, existe uma API para ser consumida e realizar todos esses procedimentos de "migration" de maneira
     simples para o usuário final. É necessário que haja a implantação dessa API em algum sistema.  </p>

       <p style="font-size: 15px; text-align: justify;">Ela possui um site bom e explicativo, porém a documentação não é tão esclarecedora, é necessário que existe noção 
       de programação para realizar a implantação desta api no sistema. </p>
       
    </div>