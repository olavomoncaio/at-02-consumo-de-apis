<?php 
    try {

        // the email of CRM user to which the Lead will be assigned 
        $userEmail = 'bill@mail.com';
        
        // new lead details
        $leadFirstName = 'Tom';
        $leadLastName = 'Ford';
       
        // search the user in CRM with a corresponding email
        $userApi = new \Data2CRMAPI\Resource\UserApi($client);
        $users = $userApi->fetchAll(50, 1, ['email.address' => $userEmail]);
       
        // the filters use Cache, so make sure that Cache is initialized; in case there is no cached data, Cache will be initialized automaticaly
        if (true === $userApi->getData()->isStatusDone()) {
            if (false === empty($users)) {
                // prepare the Lead data for recording
                $lead = new \Data2CRMAPI\Model\LeadEntity();
                $lead->setFirstName($leadFirstName);
                $lead->setLastName($leadLastName);
                $lead->setUser((new \Data2CRMAPI\Model\UserEntityRelation())->setId(current($users)->getId()));
               
                // create the Lead 
                $leadApi = new \Data2CRMAPI\Resource\LeadApi($client);
                $leadRelation = $leadApi->create($lead);
                echo 'Lead created: ' . $leadRelation->getId() . PHP_EOL;
            } else {
                // if there is no user with the specified email in the CRM, throw the error
                throw new \Exception('User with email "' . $userEmail . '" not found');
            }
        } else {
            // in case you couldn’t get cached data, show a corresponding message
            echo 'Status message: ' . $userApi->getData()->getStatusMessage() . PHP_EOL;
        }
     
     } catch (\Data2CRMAPI\ApiException $exception) {
        // show the errors, if there were any
        echo 'Failed: #'
            . $exception->getCode()
            . ' => ' . $exception->getDetailCode() . ': ' . $exception->getDetailMessage()
            . ' => ' . print_r($exception->getDetailExtra(), true) . PHP_EOL
            . PHP_EOL;
     } catch (\Exception $exception) {
        echo 'Failed: ' . $exception->getMessage() . PHP_EOL;
     }
     
?>


<h3> alou </h3>