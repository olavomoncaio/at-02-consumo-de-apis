<?php

namespace Data2CRMAPI\Model;

use \ArrayAccess;

class Phone extends AbstractModel  implements ArrayAccess
{   
    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'type' => 'string',
        'number' => 'string'
    );

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = array(
        'type' => 'type',
        'number' => 'number'
    );

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = array(
        'type' => 'setType',
        'number' => 'setNumber'
    );

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = array(
        'type' => 'getType',
        'number' => 'getNumber'
    );

    /**
     * Gets type
     *
     * @return string
     */
    public function getType()
    {
        return $this->offsetGet('type');
    }

    /**
     * Sets type
     *
     * @param string $type Type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->offsetSet('type', $type);

        return $this;
    }
    /**
     * Gets number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->offsetGet('number');
    }

    /**
     * Sets number
     *
     * @param string $number Number
     *
     * @return $this
     */
    public function setNumber($number)
    {
        $this->offsetSet('number', $number);

        return $this;
    }
}
