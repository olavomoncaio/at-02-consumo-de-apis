<?php

namespace Data2CRMAPI\Model;

use \ArrayAccess;

class CallEntity extends AbstractModel  implements ArrayAccess
{   
    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'id' => 'string',
        'direction' => 'string',
        'purpose' => 'string',
        'subject' => 'string',
        'description' => 'string',
        'status' => 'string',
        'result' => 'string',
        'startedAt' => '\DateTime',
        'endedAt' => '\DateTime',
        'parent' => '\Data2CRMAPI\Model\CallEntityRelation',
        'relation' => '\Data2CRMAPI\Model\Relation',
        'assignedUser' => '\Data2CRMAPI\Model\UserEntityRelation',
        'user' => '\Data2CRMAPI\Model\UserEntityRelation',
        'createdBy' => '\Data2CRMAPI\Model\UserEntityRelation',
        'createdAt' => '\DateTime',
        'updatedBy' => '\Data2CRMAPI\Model\UserEntityRelation',
        'updatedAt' => '\DateTime'
    );

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = array(
        'id' => 'id',
        'direction' => 'direction',
        'purpose' => 'purpose',
        'subject' => 'subject',
        'description' => 'description',
        'status' => 'status',
        'result' => 'result',
        'startedAt' => 'started_at',
        'endedAt' => 'ended_at',
        'parent' => 'parent',
        'relation' => 'relation',
        'assignedUser' => 'assigned_user',
        'user' => 'user',
        'createdBy' => 'created_by',
        'createdAt' => 'created_at',
        'updatedBy' => 'updated_by',
        'updatedAt' => 'updated_at'
    );

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = array(
        'id' => 'setId',
        'direction' => 'setDirection',
        'purpose' => 'setPurpose',
        'subject' => 'setSubject',
        'description' => 'setDescription',
        'status' => 'setStatus',
        'result' => 'setResult',
        'startedAt' => 'setStartedAt',
        'endedAt' => 'setEndedAt',
        'parent' => 'setParent',
        'relation' => 'setRelation',
        'assignedUser' => 'setAssignedUser',
        'user' => 'setUser',
        'createdBy' => 'setCreatedBy',
        'createdAt' => 'setCreatedAt',
        'updatedBy' => 'setUpdatedBy',
        'updatedAt' => 'setUpdatedAt'
    );

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = array(
        'id' => 'getId',
        'direction' => 'getDirection',
        'purpose' => 'getPurpose',
        'subject' => 'getSubject',
        'description' => 'getDescription',
        'status' => 'getStatus',
        'result' => 'getResult',
        'startedAt' => 'getStartedAt',
        'endedAt' => 'getEndedAt',
        'parent' => 'getParent',
        'relation' => 'getRelation',
        'assignedUser' => 'getAssignedUser',
        'user' => 'getUser',
        'createdBy' => 'getCreatedBy',
        'createdAt' => 'getCreatedAt',
        'updatedBy' => 'getUpdatedBy',
        'updatedAt' => 'getUpdatedAt'
    );

    /**
     * Gets id
     *
     * @return string
     */
    public function getId()
    {
        return $this->offsetGet('id');
    }

    /**
     * Sets id
     *
     * @param string $id Call Identifier
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->offsetSet('id', $id);

        return $this;
    }
    /**
     * Gets direction
     *
     * @return string
     */
    public function getDirection()
    {
        return $this->offsetGet('direction');
    }

    /**
     * Sets direction
     *
     * @param string $direction Direction
     *
     * @return $this
     */
    public function setDirection($direction)
    {
        $this->offsetSet('direction', $direction);

        return $this;
    }
    /**
     * Gets purpose
     *
     * @return string
     */
    public function getPurpose()
    {
        return $this->offsetGet('purpose');
    }

    /**
     * Sets purpose
     *
     * @param string $purpose Purpose
     *
     * @return $this
     */
    public function setPurpose($purpose)
    {
        $this->offsetSet('purpose', $purpose);

        return $this;
    }
    /**
     * Gets subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->offsetGet('subject');
    }

    /**
     * Sets subject
     *
     * @param string $subject Subject
     *
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->offsetSet('subject', $subject);

        return $this;
    }
    /**
     * Gets description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->offsetGet('description');
    }

    /**
     * Sets description
     *
     * @param string $description Description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->offsetSet('description', $description);

        return $this;
    }
    /**
     * Gets status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->offsetGet('status');
    }

    /**
     * Sets status
     *
     * @param string $status Status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->offsetSet('status', $status);

        return $this;
    }
    /**
     * Gets result
     *
     * @return string
     */
    public function getResult()
    {
        return $this->offsetGet('result');
    }

    /**
     * Sets result
     *
     * @param string $result Result
     *
     * @return $this
     */
    public function setResult($result)
    {
        $this->offsetSet('result', $result);

        return $this;
    }
    /**
     * Gets startedAt
     *
     * @return \DateTime
     */
    public function getStartedAt()
    {
        return $this->offsetGet('startedAt');
    }

    /**
     * Sets startedAt
     *
     * @param \DateTime $startedAt Started At
     *
     * @return $this
     */
    public function setStartedAt($startedAt)
    {
        $this->offsetSet('startedAt', $startedAt);

        return $this;
    }
    /**
     * Gets endedAt
     *
     * @return \DateTime
     */
    public function getEndedAt()
    {
        return $this->offsetGet('endedAt');
    }

    /**
     * Sets endedAt
     *
     * @param \DateTime $endedAt Ended At
     *
     * @return $this
     */
    public function setEndedAt($endedAt)
    {
        $this->offsetSet('endedAt', $endedAt);

        return $this;
    }
    /**
     * Gets parent
     *
     * @return \Data2CRMAPI\Model\CallEntityRelation
     */
    public function getParent()
    {
        return $this->offsetGet('parent');
    }

    /**
     * Sets parent
     *
     * @param \Data2CRMAPI\Model\CallEntityRelation $parent Parent
     *
     * @return $this
     */
    public function setParent($parent)
    {
        $this->offsetSet('parent', $parent);

        return $this;
    }
    /**
     * Gets relation
     *
     * @return \Data2CRMAPI\Model\Relation
     */
    public function getRelation()
    {
        return $this->offsetGet('relation');
    }

    /**
     * Sets relation
     *
     * @param \Data2CRMAPI\Model\Relation $relation Relation
     *
     * @return $this
     */
    public function setRelation($relation)
    {
        $this->offsetSet('relation', $relation);

        return $this;
    }
    /**
     * Gets assignedUser
     *
     * @return \Data2CRMAPI\Model\UserEntityRelation
     */
    public function getAssignedUser()
    {
        return $this->offsetGet('assignedUser');
    }

    /**
     * Sets assignedUser
     *
     * @param \Data2CRMAPI\Model\UserEntityRelation $assignedUser Assigned User
     *
     * @return $this
     */
    public function setAssignedUser($assignedUser)
    {
        $this->offsetSet('assignedUser', $assignedUser);

        return $this;
    }
    /**
     * Gets user
     *
     * @return \Data2CRMAPI\Model\UserEntityRelation
     */
    public function getUser()
    {
        return $this->offsetGet('user');
    }

    /**
     * Sets user
     *
     * @param \Data2CRMAPI\Model\UserEntityRelation $user User
     *
     * @return $this
     */
    public function setUser($user)
    {
        $this->offsetSet('user', $user);

        return $this;
    }
    /**
     * Gets createdBy
     *
     * @return \Data2CRMAPI\Model\UserEntityRelation
     */
    public function getCreatedBy()
    {
        return $this->offsetGet('createdBy');
    }

    /**
     * Sets createdBy
     *
     * @param \Data2CRMAPI\Model\UserEntityRelation $createdBy Created By
     *
     * @return $this
     */
    public function setCreatedBy($createdBy)
    {
        $this->offsetSet('createdBy', $createdBy);

        return $this;
    }
    /**
     * Gets createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->offsetGet('createdAt');
    }

    /**
     * Sets createdAt
     *
     * @param \DateTime $createdAt Created At
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->offsetSet('createdAt', $createdAt);

        return $this;
    }
    /**
     * Gets updatedBy
     *
     * @return \Data2CRMAPI\Model\UserEntityRelation
     */
    public function getUpdatedBy()
    {
        return $this->offsetGet('updatedBy');
    }

    /**
     * Sets updatedBy
     *
     * @param \Data2CRMAPI\Model\UserEntityRelation $updatedBy Updated By
     *
     * @return $this
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->offsetSet('updatedBy', $updatedBy);

        return $this;
    }
    /**
     * Gets updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->offsetGet('updatedAt');
    }

    /**
     * Sets updatedAt
     *
     * @param \DateTime $updatedAt Updated At
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->offsetSet('updatedAt', $updatedAt);

        return $this;
    }
}
