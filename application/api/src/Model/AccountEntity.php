<?php

namespace Data2CRMAPI\Model;

use \ArrayAccess;

class AccountEntity extends AbstractModel  implements ArrayAccess
{   
    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'id' => 'string',
        'type' => 'string',
        'name' => 'string',
        'tickerSymbol' => 'string',
        'employees' => 'string',
        'ownership' => 'string',
        'industry' => 'string',
        'description' => 'string',
        'annualRevenue' => 'string',
        'sicCode' => 'string',
        'rating' => 'string',
        'category' => 'string',
        'email' => '\Data2CRMAPI\Model\Email[]',
        'phone' => '\Data2CRMAPI\Model\Phone[]',
        'messenger' => '\Data2CRMAPI\Model\Messenger[]',
        'website' => '\Data2CRMAPI\Model\Website[]',
        'address' => '\Data2CRMAPI\Model\Address[]',
        'parent' => '\Data2CRMAPI\Model\AccountEntityRelation',
        'relation' => '\Data2CRMAPI\Model\Relation',
        'assignedUser' => '\Data2CRMAPI\Model\UserEntityRelation',
        'user' => '\Data2CRMAPI\Model\UserEntityRelation',
        'createdBy' => '\Data2CRMAPI\Model\UserEntityRelation',
        'createdAt' => '\DateTime',
        'updatedBy' => '\Data2CRMAPI\Model\UserEntityRelation',
        'updatedAt' => '\DateTime'
    );

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = array(
        'id' => 'id',
        'type' => 'type',
        'name' => 'name',
        'tickerSymbol' => 'ticker_symbol',
        'employees' => 'employees',
        'ownership' => 'ownership',
        'industry' => 'industry',
        'description' => 'description',
        'annualRevenue' => 'annual_revenue',
        'sicCode' => 'sic_code',
        'rating' => 'rating',
        'category' => 'category',
        'email' => 'email',
        'phone' => 'phone',
        'messenger' => 'messenger',
        'website' => 'website',
        'address' => 'address',
        'parent' => 'parent',
        'relation' => 'relation',
        'assignedUser' => 'assigned_user',
        'user' => 'user',
        'createdBy' => 'created_by',
        'createdAt' => 'created_at',
        'updatedBy' => 'updated_by',
        'updatedAt' => 'updated_at'
    );

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = array(
        'id' => 'setId',
        'type' => 'setType',
        'name' => 'setName',
        'tickerSymbol' => 'setTickerSymbol',
        'employees' => 'setEmployees',
        'ownership' => 'setOwnership',
        'industry' => 'setIndustry',
        'description' => 'setDescription',
        'annualRevenue' => 'setAnnualRevenue',
        'sicCode' => 'setSicCode',
        'rating' => 'setRating',
        'category' => 'setCategory',
        'email' => 'setEmail',
        'phone' => 'setPhone',
        'messenger' => 'setMessenger',
        'website' => 'setWebsite',
        'address' => 'setAddress',
        'parent' => 'setParent',
        'relation' => 'setRelation',
        'assignedUser' => 'setAssignedUser',
        'user' => 'setUser',
        'createdBy' => 'setCreatedBy',
        'createdAt' => 'setCreatedAt',
        'updatedBy' => 'setUpdatedBy',
        'updatedAt' => 'setUpdatedAt'
    );

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = array(
        'id' => 'getId',
        'type' => 'getType',
        'name' => 'getName',
        'tickerSymbol' => 'getTickerSymbol',
        'employees' => 'getEmployees',
        'ownership' => 'getOwnership',
        'industry' => 'getIndustry',
        'description' => 'getDescription',
        'annualRevenue' => 'getAnnualRevenue',
        'sicCode' => 'getSicCode',
        'rating' => 'getRating',
        'category' => 'getCategory',
        'email' => 'getEmail',
        'phone' => 'getPhone',
        'messenger' => 'getMessenger',
        'website' => 'getWebsite',
        'address' => 'getAddress',
        'parent' => 'getParent',
        'relation' => 'getRelation',
        'assignedUser' => 'getAssignedUser',
        'user' => 'getUser',
        'createdBy' => 'getCreatedBy',
        'createdAt' => 'getCreatedAt',
        'updatedBy' => 'getUpdatedBy',
        'updatedAt' => 'getUpdatedAt'
    );

    /**
     * Gets id
     *
     * @return string
     */
    public function getId()
    {
        return $this->offsetGet('id');
    }

    /**
     * Sets id
     *
     * @param string $id Account Identifier
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->offsetSet('id', $id);

        return $this;
    }
    /**
     * Gets type
     *
     * @return string
     */
    public function getType()
    {
        return $this->offsetGet('type');
    }

    /**
     * Sets type
     *
     * @param string $type Type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->offsetSet('type', $type);

        return $this;
    }
    /**
     * Gets name
     *
     * @return string
     */
    public function getName()
    {
        return $this->offsetGet('name');
    }

    /**
     * Sets name
     *
     * @param string $name Name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->offsetSet('name', $name);

        return $this;
    }
    /**
     * Gets tickerSymbol
     *
     * @return string
     */
    public function getTickerSymbol()
    {
        return $this->offsetGet('tickerSymbol');
    }

    /**
     * Sets tickerSymbol
     *
     * @param string $tickerSymbol Ticker Symbol
     *
     * @return $this
     */
    public function setTickerSymbol($tickerSymbol)
    {
        $this->offsetSet('tickerSymbol', $tickerSymbol);

        return $this;
    }
    /**
     * Gets employees
     *
     * @return string
     */
    public function getEmployees()
    {
        return $this->offsetGet('employees');
    }

    /**
     * Sets employees
     *
     * @param string $employees Employees
     *
     * @return $this
     */
    public function setEmployees($employees)
    {
        $this->offsetSet('employees', $employees);

        return $this;
    }
    /**
     * Gets ownership
     *
     * @return string
     */
    public function getOwnership()
    {
        return $this->offsetGet('ownership');
    }

    /**
     * Sets ownership
     *
     * @param string $ownership Ownership
     *
     * @return $this
     */
    public function setOwnership($ownership)
    {
        $this->offsetSet('ownership', $ownership);

        return $this;
    }
    /**
     * Gets industry
     *
     * @return string
     */
    public function getIndustry()
    {
        return $this->offsetGet('industry');
    }

    /**
     * Sets industry
     *
     * @param string $industry Industry
     *
     * @return $this
     */
    public function setIndustry($industry)
    {
        $this->offsetSet('industry', $industry);

        return $this;
    }
    /**
     * Gets description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->offsetGet('description');
    }

    /**
     * Sets description
     *
     * @param string $description Description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->offsetSet('description', $description);

        return $this;
    }
    /**
     * Gets annualRevenue
     *
     * @return string
     */
    public function getAnnualRevenue()
    {
        return $this->offsetGet('annualRevenue');
    }

    /**
     * Sets annualRevenue
     *
     * @param string $annualRevenue Annual Revenue
     *
     * @return $this
     */
    public function setAnnualRevenue($annualRevenue)
    {
        $this->offsetSet('annualRevenue', $annualRevenue);

        return $this;
    }
    /**
     * Gets sicCode
     *
     * @return string
     */
    public function getSicCode()
    {
        return $this->offsetGet('sicCode');
    }

    /**
     * Sets sicCode
     *
     * @param string $sicCode SIC Code
     *
     * @return $this
     */
    public function setSicCode($sicCode)
    {
        $this->offsetSet('sicCode', $sicCode);

        return $this;
    }
    /**
     * Gets rating
     *
     * @return string
     */
    public function getRating()
    {
        return $this->offsetGet('rating');
    }

    /**
     * Sets rating
     *
     * @param string $rating Rating
     *
     * @return $this
     */
    public function setRating($rating)
    {
        $this->offsetSet('rating', $rating);

        return $this;
    }
    /**
     * Gets category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->offsetGet('category');
    }

    /**
     * Sets category
     *
     * @param string $category Category
     *
     * @return $this
     */
    public function setCategory($category)
    {
        $this->offsetSet('category', $category);

        return $this;
    }
    /**
     * Gets email
     *
     * @return \Data2CRMAPI\Model\Email[]
     */
    public function getEmail()
    {
        return $this->offsetGet('email');
    }

    /**
     * Sets email
     *
     * @param \Data2CRMAPI\Model\Email[] $email Email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->offsetSet('email', $email);

        return $this;
    }
    /**
     * Gets phone
     *
     * @return \Data2CRMAPI\Model\Phone[]
     */
    public function getPhone()
    {
        return $this->offsetGet('phone');
    }

    /**
     * Sets phone
     *
     * @param \Data2CRMAPI\Model\Phone[] $phone Phone
     *
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->offsetSet('phone', $phone);

        return $this;
    }
    /**
     * Gets messenger
     *
     * @return \Data2CRMAPI\Model\Messenger[]
     */
    public function getMessenger()
    {
        return $this->offsetGet('messenger');
    }

    /**
     * Sets messenger
     *
     * @param \Data2CRMAPI\Model\Messenger[] $messenger Messenger
     *
     * @return $this
     */
    public function setMessenger($messenger)
    {
        $this->offsetSet('messenger', $messenger);

        return $this;
    }
    /**
     * Gets website
     *
     * @return \Data2CRMAPI\Model\Website[]
     */
    public function getWebsite()
    {
        return $this->offsetGet('website');
    }

    /**
     * Sets website
     *
     * @param \Data2CRMAPI\Model\Website[] $website Website
     *
     * @return $this
     */
    public function setWebsite($website)
    {
        $this->offsetSet('website', $website);

        return $this;
    }
    /**
     * Gets address
     *
     * @return \Data2CRMAPI\Model\Address[]
     */
    public function getAddress()
    {
        return $this->offsetGet('address');
    }

    /**
     * Sets address
     *
     * @param \Data2CRMAPI\Model\Address[] $address Address
     *
     * @return $this
     */
    public function setAddress($address)
    {
        $this->offsetSet('address', $address);

        return $this;
    }
    /**
     * Gets parent
     *
     * @return \Data2CRMAPI\Model\AccountEntityRelation
     */
    public function getParent()
    {
        return $this->offsetGet('parent');
    }

    /**
     * Sets parent
     *
     * @param \Data2CRMAPI\Model\AccountEntityRelation $parent Parent
     *
     * @return $this
     */
    public function setParent($parent)
    {
        $this->offsetSet('parent', $parent);

        return $this;
    }
    /**
     * Gets relation
     *
     * @return \Data2CRMAPI\Model\Relation
     */
    public function getRelation()
    {
        return $this->offsetGet('relation');
    }

    /**
     * Sets relation
     *
     * @param \Data2CRMAPI\Model\Relation $relation Relation
     *
     * @return $this
     */
    public function setRelation($relation)
    {
        $this->offsetSet('relation', $relation);

        return $this;
    }
    /**
     * Gets assignedUser
     *
     * @return \Data2CRMAPI\Model\UserEntityRelation
     */
    public function getAssignedUser()
    {
        return $this->offsetGet('assignedUser');
    }

    /**
     * Sets assignedUser
     *
     * @param \Data2CRMAPI\Model\UserEntityRelation $assignedUser Assigned User
     *
     * @return $this
     */
    public function setAssignedUser($assignedUser)
    {
        $this->offsetSet('assignedUser', $assignedUser);

        return $this;
    }
    /**
     * Gets user
     *
     * @return \Data2CRMAPI\Model\UserEntityRelation
     */
    public function getUser()
    {
        return $this->offsetGet('user');
    }

    /**
     * Sets user
     *
     * @param \Data2CRMAPI\Model\UserEntityRelation $user User
     *
     * @return $this
     */
    public function setUser($user)
    {
        $this->offsetSet('user', $user);

        return $this;
    }
    /**
     * Gets createdBy
     *
     * @return \Data2CRMAPI\Model\UserEntityRelation
     */
    public function getCreatedBy()
    {
        return $this->offsetGet('createdBy');
    }

    /**
     * Sets createdBy
     *
     * @param \Data2CRMAPI\Model\UserEntityRelation $createdBy Created By
     *
     * @return $this
     */
    public function setCreatedBy($createdBy)
    {
        $this->offsetSet('createdBy', $createdBy);

        return $this;
    }
    /**
     * Gets createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->offsetGet('createdAt');
    }

    /**
     * Sets createdAt
     *
     * @param \DateTime $createdAt Created At
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->offsetSet('createdAt', $createdAt);

        return $this;
    }
    /**
     * Gets updatedBy
     *
     * @return \Data2CRMAPI\Model\UserEntityRelation
     */
    public function getUpdatedBy()
    {
        return $this->offsetGet('updatedBy');
    }

    /**
     * Sets updatedBy
     *
     * @param \Data2CRMAPI\Model\UserEntityRelation $updatedBy Updated By
     *
     * @return $this
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->offsetSet('updatedBy', $updatedBy);

        return $this;
    }
    /**
     * Gets updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->offsetGet('updatedAt');
    }

    /**
     * Sets updatedAt
     *
     * @param \DateTime $updatedAt Updated At
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->offsetSet('updatedAt', $updatedAt);

        return $this;
    }
}
