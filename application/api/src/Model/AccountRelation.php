<?php

namespace Data2CRMAPI\Model;

use \ArrayAccess;

class AccountRelation extends AbstractModel  implements ArrayAccess
{   
    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'type' => 'string',
        'entity' => '\Data2CRMAPI\Model\AccountEntityRelation'
    );

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = array(
        'type' => 'type',
        'entity' => 'entity'
    );

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = array(
        'type' => 'setType',
        'entity' => 'setEntity'
    );

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = array(
        'type' => 'getType',
        'entity' => 'getEntity'
    );

    /**
     * Gets type
     *
     * @return string
     */
    public function getType()
    {
        return $this->offsetGet('type');
    }

    /**
     * Sets type
     *
     * @param string $type Type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->offsetSet('type', $type);

        return $this;
    }
    /**
     * Gets entity
     *
     * @return \Data2CRMAPI\Model\AccountEntityRelation
     */
    public function getEntity()
    {
        return $this->offsetGet('entity');
    }

    /**
     * Sets entity
     *
     * @param \Data2CRMAPI\Model\AccountEntityRelation $entity Entity
     *
     * @return $this
     */
    public function setEntity($entity)
    {
        $this->offsetSet('entity', $entity);

        return $this;
    }
}
