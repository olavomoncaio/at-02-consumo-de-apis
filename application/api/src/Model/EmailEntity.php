<?php

namespace Data2CRMAPI\Model;

use \ArrayAccess;

class EmailEntity extends AbstractModel  implements ArrayAccess
{   
    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'id' => 'string',
        'direction' => 'string',
        'from' => 'string',
        'to' => 'string',
        'cc' => 'string',
        'bcc' => 'string',
        'subject' => 'string',
        'body' => 'string',
        'status' => 'string',
        'sentAt' => '\DateTime',
        'parent' => '\Data2CRMAPI\Model\EmailEntityRelation',
        'relation' => '\Data2CRMAPI\Model\Relation',
        'assignedUser' => '\Data2CRMAPI\Model\UserEntityRelation',
        'user' => '\Data2CRMAPI\Model\UserEntityRelation',
        'createdBy' => '\Data2CRMAPI\Model\UserEntityRelation',
        'createdAt' => '\DateTime',
        'updatedBy' => '\Data2CRMAPI\Model\UserEntityRelation',
        'updatedAt' => '\DateTime'
    );

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = array(
        'id' => 'id',
        'direction' => 'direction',
        'from' => 'from',
        'to' => 'to',
        'cc' => 'cc',
        'bcc' => 'bcc',
        'subject' => 'subject',
        'body' => 'body',
        'status' => 'status',
        'sentAt' => 'sent_at',
        'parent' => 'parent',
        'relation' => 'relation',
        'assignedUser' => 'assigned_user',
        'user' => 'user',
        'createdBy' => 'created_by',
        'createdAt' => 'created_at',
        'updatedBy' => 'updated_by',
        'updatedAt' => 'updated_at'
    );

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = array(
        'id' => 'setId',
        'direction' => 'setDirection',
        'from' => 'setFrom',
        'to' => 'setTo',
        'cc' => 'setCc',
        'bcc' => 'setBcc',
        'subject' => 'setSubject',
        'body' => 'setBody',
        'status' => 'setStatus',
        'sentAt' => 'setSentAt',
        'parent' => 'setParent',
        'relation' => 'setRelation',
        'assignedUser' => 'setAssignedUser',
        'user' => 'setUser',
        'createdBy' => 'setCreatedBy',
        'createdAt' => 'setCreatedAt',
        'updatedBy' => 'setUpdatedBy',
        'updatedAt' => 'setUpdatedAt'
    );

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = array(
        'id' => 'getId',
        'direction' => 'getDirection',
        'from' => 'getFrom',
        'to' => 'getTo',
        'cc' => 'getCc',
        'bcc' => 'getBcc',
        'subject' => 'getSubject',
        'body' => 'getBody',
        'status' => 'getStatus',
        'sentAt' => 'getSentAt',
        'parent' => 'getParent',
        'relation' => 'getRelation',
        'assignedUser' => 'getAssignedUser',
        'user' => 'getUser',
        'createdBy' => 'getCreatedBy',
        'createdAt' => 'getCreatedAt',
        'updatedBy' => 'getUpdatedBy',
        'updatedAt' => 'getUpdatedAt'
    );

    /**
     * Gets id
     *
     * @return string
     */
    public function getId()
    {
        return $this->offsetGet('id');
    }

    /**
     * Sets id
     *
     * @param string $id Email Identifier
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->offsetSet('id', $id);

        return $this;
    }
    /**
     * Gets direction
     *
     * @return string
     */
    public function getDirection()
    {
        return $this->offsetGet('direction');
    }

    /**
     * Sets direction
     *
     * @param string $direction Direction
     *
     * @return $this
     */
    public function setDirection($direction)
    {
        $this->offsetSet('direction', $direction);

        return $this;
    }
    /**
     * Gets from
     *
     * @return string
     */
    public function getFrom()
    {
        return $this->offsetGet('from');
    }

    /**
     * Sets from
     *
     * @param string $from From
     *
     * @return $this
     */
    public function setFrom($from)
    {
        $this->offsetSet('from', $from);

        return $this;
    }
    /**
     * Gets to
     *
     * @return string
     */
    public function getTo()
    {
        return $this->offsetGet('to');
    }

    /**
     * Sets to
     *
     * @param string $to To
     *
     * @return $this
     */
    public function setTo($to)
    {
        $this->offsetSet('to', $to);

        return $this;
    }
    /**
     * Gets cc
     *
     * @return string
     */
    public function getCc()
    {
        return $this->offsetGet('cc');
    }

    /**
     * Sets cc
     *
     * @param string $cc CC
     *
     * @return $this
     */
    public function setCc($cc)
    {
        $this->offsetSet('cc', $cc);

        return $this;
    }
    /**
     * Gets bcc
     *
     * @return string
     */
    public function getBcc()
    {
        return $this->offsetGet('bcc');
    }

    /**
     * Sets bcc
     *
     * @param string $bcc BCC
     *
     * @return $this
     */
    public function setBcc($bcc)
    {
        $this->offsetSet('bcc', $bcc);

        return $this;
    }
    /**
     * Gets subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->offsetGet('subject');
    }

    /**
     * Sets subject
     *
     * @param string $subject Subject
     *
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->offsetSet('subject', $subject);

        return $this;
    }
    /**
     * Gets body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->offsetGet('body');
    }

    /**
     * Sets body
     *
     * @param string $body Body
     *
     * @return $this
     */
    public function setBody($body)
    {
        $this->offsetSet('body', $body);

        return $this;
    }
    /**
     * Gets status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->offsetGet('status');
    }

    /**
     * Sets status
     *
     * @param string $status Status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->offsetSet('status', $status);

        return $this;
    }
    /**
     * Gets sentAt
     *
     * @return \DateTime
     */
    public function getSentAt()
    {
        return $this->offsetGet('sentAt');
    }

    /**
     * Sets sentAt
     *
     * @param \DateTime $sentAt Sent At
     *
     * @return $this
     */
    public function setSentAt($sentAt)
    {
        $this->offsetSet('sentAt', $sentAt);

        return $this;
    }
    /**
     * Gets parent
     *
     * @return \Data2CRMAPI\Model\EmailEntityRelation
     */
    public function getParent()
    {
        return $this->offsetGet('parent');
    }

    /**
     * Sets parent
     *
     * @param \Data2CRMAPI\Model\EmailEntityRelation $parent Parent
     *
     * @return $this
     */
    public function setParent($parent)
    {
        $this->offsetSet('parent', $parent);

        return $this;
    }
    /**
     * Gets relation
     *
     * @return \Data2CRMAPI\Model\Relation
     */
    public function getRelation()
    {
        return $this->offsetGet('relation');
    }

    /**
     * Sets relation
     *
     * @param \Data2CRMAPI\Model\Relation $relation Relation
     *
     * @return $this
     */
    public function setRelation($relation)
    {
        $this->offsetSet('relation', $relation);

        return $this;
    }
    /**
     * Gets assignedUser
     *
     * @return \Data2CRMAPI\Model\UserEntityRelation
     */
    public function getAssignedUser()
    {
        return $this->offsetGet('assignedUser');
    }

    /**
     * Sets assignedUser
     *
     * @param \Data2CRMAPI\Model\UserEntityRelation $assignedUser Assigned User
     *
     * @return $this
     */
    public function setAssignedUser($assignedUser)
    {
        $this->offsetSet('assignedUser', $assignedUser);

        return $this;
    }
    /**
     * Gets user
     *
     * @return \Data2CRMAPI\Model\UserEntityRelation
     */
    public function getUser()
    {
        return $this->offsetGet('user');
    }

    /**
     * Sets user
     *
     * @param \Data2CRMAPI\Model\UserEntityRelation $user User
     *
     * @return $this
     */
    public function setUser($user)
    {
        $this->offsetSet('user', $user);

        return $this;
    }
    /**
     * Gets createdBy
     *
     * @return \Data2CRMAPI\Model\UserEntityRelation
     */
    public function getCreatedBy()
    {
        return $this->offsetGet('createdBy');
    }

    /**
     * Sets createdBy
     *
     * @param \Data2CRMAPI\Model\UserEntityRelation $createdBy Created By
     *
     * @return $this
     */
    public function setCreatedBy($createdBy)
    {
        $this->offsetSet('createdBy', $createdBy);

        return $this;
    }
    /**
     * Gets createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->offsetGet('createdAt');
    }

    /**
     * Sets createdAt
     *
     * @param \DateTime $createdAt Created At
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->offsetSet('createdAt', $createdAt);

        return $this;
    }
    /**
     * Gets updatedBy
     *
     * @return \Data2CRMAPI\Model\UserEntityRelation
     */
    public function getUpdatedBy()
    {
        return $this->offsetGet('updatedBy');
    }

    /**
     * Sets updatedBy
     *
     * @param \Data2CRMAPI\Model\UserEntityRelation $updatedBy Updated By
     *
     * @return $this
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->offsetSet('updatedBy', $updatedBy);

        return $this;
    }
    /**
     * Gets updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->offsetGet('updatedAt');
    }

    /**
     * Sets updatedAt
     *
     * @param \DateTime $updatedAt Updated At
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->offsetSet('updatedAt', $updatedAt);

        return $this;
    }
}
