<?php

namespace Data2CRMAPI\Model;

use \ArrayAccess;

class LeadEntity extends AbstractModel  implements ArrayAccess
{   
    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'id' => 'string',
        'salutation' => 'string',
        'firstName' => 'string',
        'middleName' => 'string',
        'lastName' => 'string',
        'nameSuffix' => 'string',
        'description' => 'string',
        'position' => 'string',
        'department' => 'string',
        'company' => 'string',
        'industry' => 'string',
        'annualRevenue' => 'string',
        'birthDate' => '\DateTime',
        'type' => 'string',
        'rating' => 'string',
        'source' => 'string',
        'sourceDescription' => 'string',
        'status' => 'string',
        'statusDescription' => 'string',
        'doNotCall' => 'bool',
        'email' => '\Data2CRMAPI\Model\Email[]',
        'phone' => '\Data2CRMAPI\Model\Phone[]',
        'messenger' => '\Data2CRMAPI\Model\Messenger[]',
        'website' => '\Data2CRMAPI\Model\Website[]',
        'address' => '\Data2CRMAPI\Model\Address[]',
        'parent' => '\Data2CRMAPI\Model\LeadEntityRelation',
        'relation' => '\Data2CRMAPI\Model\Relation',
        'assignedUser' => '\Data2CRMAPI\Model\UserEntityRelation',
        'user' => '\Data2CRMAPI\Model\UserEntityRelation',
        'createdBy' => '\Data2CRMAPI\Model\UserEntityRelation',
        'createdAt' => '\DateTime',
        'updatedBy' => '\Data2CRMAPI\Model\UserEntityRelation',
        'updatedAt' => '\DateTime'
    );

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = array(
        'id' => 'id',
        'salutation' => 'salutation',
        'firstName' => 'first_name',
        'middleName' => 'middle_name',
        'lastName' => 'last_name',
        'nameSuffix' => 'name_suffix',
        'description' => 'description',
        'position' => 'position',
        'department' => 'department',
        'company' => 'company',
        'industry' => 'industry',
        'annualRevenue' => 'annual_revenue',
        'birthDate' => 'birth_date',
        'type' => 'type',
        'rating' => 'rating',
        'source' => 'source',
        'sourceDescription' => 'source_description',
        'status' => 'status',
        'statusDescription' => 'status_description',
        'doNotCall' => 'do_not_call',
        'email' => 'email',
        'phone' => 'phone',
        'messenger' => 'messenger',
        'website' => 'website',
        'address' => 'address',
        'parent' => 'parent',
        'relation' => 'relation',
        'assignedUser' => 'assigned_user',
        'user' => 'user',
        'createdBy' => 'created_by',
        'createdAt' => 'created_at',
        'updatedBy' => 'updated_by',
        'updatedAt' => 'updated_at'
    );

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = array(
        'id' => 'setId',
        'salutation' => 'setSalutation',
        'firstName' => 'setFirstName',
        'middleName' => 'setMiddleName',
        'lastName' => 'setLastName',
        'nameSuffix' => 'setNameSuffix',
        'description' => 'setDescription',
        'position' => 'setPosition',
        'department' => 'setDepartment',
        'company' => 'setCompany',
        'industry' => 'setIndustry',
        'annualRevenue' => 'setAnnualRevenue',
        'birthDate' => 'setBirthDate',
        'type' => 'setType',
        'rating' => 'setRating',
        'source' => 'setSource',
        'sourceDescription' => 'setSourceDescription',
        'status' => 'setStatus',
        'statusDescription' => 'setStatusDescription',
        'doNotCall' => 'setDoNotCall',
        'email' => 'setEmail',
        'phone' => 'setPhone',
        'messenger' => 'setMessenger',
        'website' => 'setWebsite',
        'address' => 'setAddress',
        'parent' => 'setParent',
        'relation' => 'setRelation',
        'assignedUser' => 'setAssignedUser',
        'user' => 'setUser',
        'createdBy' => 'setCreatedBy',
        'createdAt' => 'setCreatedAt',
        'updatedBy' => 'setUpdatedBy',
        'updatedAt' => 'setUpdatedAt'
    );

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = array(
        'id' => 'getId',
        'salutation' => 'getSalutation',
        'firstName' => 'getFirstName',
        'middleName' => 'getMiddleName',
        'lastName' => 'getLastName',
        'nameSuffix' => 'getNameSuffix',
        'description' => 'getDescription',
        'position' => 'getPosition',
        'department' => 'getDepartment',
        'company' => 'getCompany',
        'industry' => 'getIndustry',
        'annualRevenue' => 'getAnnualRevenue',
        'birthDate' => 'getBirthDate',
        'type' => 'getType',
        'rating' => 'getRating',
        'source' => 'getSource',
        'sourceDescription' => 'getSourceDescription',
        'status' => 'getStatus',
        'statusDescription' => 'getStatusDescription',
        'doNotCall' => 'getDoNotCall',
        'email' => 'getEmail',
        'phone' => 'getPhone',
        'messenger' => 'getMessenger',
        'website' => 'getWebsite',
        'address' => 'getAddress',
        'parent' => 'getParent',
        'relation' => 'getRelation',
        'assignedUser' => 'getAssignedUser',
        'user' => 'getUser',
        'createdBy' => 'getCreatedBy',
        'createdAt' => 'getCreatedAt',
        'updatedBy' => 'getUpdatedBy',
        'updatedAt' => 'getUpdatedAt'
    );

    /**
     * Gets id
     *
     * @return string
     */
    public function getId()
    {
        return $this->offsetGet('id');
    }

    /**
     * Sets id
     *
     * @param string $id Lead Identifier
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->offsetSet('id', $id);

        return $this;
    }
    /**
     * Gets salutation
     *
     * @return string
     */
    public function getSalutation()
    {
        return $this->offsetGet('salutation');
    }

    /**
     * Sets salutation
     *
     * @param string $salutation Salutation
     *
     * @return $this
     */
    public function setSalutation($salutation)
    {
        $this->offsetSet('salutation', $salutation);

        return $this;
    }
    /**
     * Gets firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->offsetGet('firstName');
    }

    /**
     * Sets firstName
     *
     * @param string $firstName First Name
     *
     * @return $this
     */
    public function setFirstName($firstName)
    {
        $this->offsetSet('firstName', $firstName);

        return $this;
    }
    /**
     * Gets middleName
     *
     * @return string
     */
    public function getMiddleName()
    {
        return $this->offsetGet('middleName');
    }

    /**
     * Sets middleName
     *
     * @param string $middleName Middle Name
     *
     * @return $this
     */
    public function setMiddleName($middleName)
    {
        $this->offsetSet('middleName', $middleName);

        return $this;
    }
    /**
     * Gets lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->offsetGet('lastName');
    }

    /**
     * Sets lastName
     *
     * @param string $lastName Last Name
     *
     * @return $this
     */
    public function setLastName($lastName)
    {
        $this->offsetSet('lastName', $lastName);

        return $this;
    }
    /**
     * Gets nameSuffix
     *
     * @return string
     */
    public function getNameSuffix()
    {
        return $this->offsetGet('nameSuffix');
    }

    /**
     * Sets nameSuffix
     *
     * @param string $nameSuffix Name Suffix
     *
     * @return $this
     */
    public function setNameSuffix($nameSuffix)
    {
        $this->offsetSet('nameSuffix', $nameSuffix);

        return $this;
    }
    /**
     * Gets description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->offsetGet('description');
    }

    /**
     * Sets description
     *
     * @param string $description Description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->offsetSet('description', $description);

        return $this;
    }
    /**
     * Gets position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->offsetGet('position');
    }

    /**
     * Sets position
     *
     * @param string $position Position (job)
     *
     * @return $this
     */
    public function setPosition($position)
    {
        $this->offsetSet('position', $position);

        return $this;
    }
    /**
     * Gets department
     *
     * @return string
     */
    public function getDepartment()
    {
        return $this->offsetGet('department');
    }

    /**
     * Sets department
     *
     * @param string $department Department
     *
     * @return $this
     */
    public function setDepartment($department)
    {
        $this->offsetSet('department', $department);

        return $this;
    }
    /**
     * Gets company
     *
     * @return string
     */
    public function getCompany()
    {
        return $this->offsetGet('company');
    }

    /**
     * Sets company
     *
     * @param string $company Company
     *
     * @return $this
     */
    public function setCompany($company)
    {
        $this->offsetSet('company', $company);

        return $this;
    }
    /**
     * Gets industry
     *
     * @return string
     */
    public function getIndustry()
    {
        return $this->offsetGet('industry');
    }

    /**
     * Sets industry
     *
     * @param string $industry Industry
     *
     * @return $this
     */
    public function setIndustry($industry)
    {
        $this->offsetSet('industry', $industry);

        return $this;
    }
    /**
     * Gets annualRevenue
     *
     * @return string
     */
    public function getAnnualRevenue()
    {
        return $this->offsetGet('annualRevenue');
    }

    /**
     * Sets annualRevenue
     *
     * @param string $annualRevenue Annual Revenue
     *
     * @return $this
     */
    public function setAnnualRevenue($annualRevenue)
    {
        $this->offsetSet('annualRevenue', $annualRevenue);

        return $this;
    }
    /**
     * Gets birthDate
     *
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->offsetGet('birthDate');
    }

    /**
     * Sets birthDate
     *
     * @param \DateTime $birthDate Birth Date
     *
     * @return $this
     */
    public function setBirthDate($birthDate)
    {
        $this->offsetSet('birthDate', $birthDate);

        return $this;
    }
    /**
     * Gets type
     *
     * @return string
     */
    public function getType()
    {
        return $this->offsetGet('type');
    }

    /**
     * Sets type
     *
     * @param string $type Type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->offsetSet('type', $type);

        return $this;
    }
    /**
     * Gets rating
     *
     * @return string
     */
    public function getRating()
    {
        return $this->offsetGet('rating');
    }

    /**
     * Sets rating
     *
     * @param string $rating Rating
     *
     * @return $this
     */
    public function setRating($rating)
    {
        $this->offsetSet('rating', $rating);

        return $this;
    }
    /**
     * Gets source
     *
     * @return string
     */
    public function getSource()
    {
        return $this->offsetGet('source');
    }

    /**
     * Sets source
     *
     * @param string $source Lead Source
     *
     * @return $this
     */
    public function setSource($source)
    {
        $this->offsetSet('source', $source);

        return $this;
    }
    /**
     * Gets sourceDescription
     *
     * @return string
     */
    public function getSourceDescription()
    {
        return $this->offsetGet('sourceDescription');
    }

    /**
     * Sets sourceDescription
     *
     * @param string $sourceDescription Lead Source Description
     *
     * @return $this
     */
    public function setSourceDescription($sourceDescription)
    {
        $this->offsetSet('sourceDescription', $sourceDescription);

        return $this;
    }
    /**
     * Gets status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->offsetGet('status');
    }

    /**
     * Sets status
     *
     * @param string $status Status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->offsetSet('status', $status);

        return $this;
    }
    /**
     * Gets statusDescription
     *
     * @return string
     */
    public function getStatusDescription()
    {
        return $this->offsetGet('statusDescription');
    }

    /**
     * Sets statusDescription
     *
     * @param string $statusDescription Status Description
     *
     * @return $this
     */
    public function setStatusDescription($statusDescription)
    {
        $this->offsetSet('statusDescription', $statusDescription);

        return $this;
    }
    /**
     * Gets doNotCall
     *
     * @return bool
     */
    public function getDoNotCall()
    {
        return $this->offsetGet('doNotCall');
    }

    /**
     * Sets doNotCall
     *
     * @param bool $doNotCall Do Not Call
     *
     * @return $this
     */
    public function setDoNotCall($doNotCall)
    {
        $this->offsetSet('doNotCall', $doNotCall);

        return $this;
    }
    /**
     * Gets email
     *
     * @return \Data2CRMAPI\Model\Email[]
     */
    public function getEmail()
    {
        return $this->offsetGet('email');
    }

    /**
     * Sets email
     *
     * @param \Data2CRMAPI\Model\Email[] $email Email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->offsetSet('email', $email);

        return $this;
    }
    /**
     * Gets phone
     *
     * @return \Data2CRMAPI\Model\Phone[]
     */
    public function getPhone()
    {
        return $this->offsetGet('phone');
    }

    /**
     * Sets phone
     *
     * @param \Data2CRMAPI\Model\Phone[] $phone Phone
     *
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->offsetSet('phone', $phone);

        return $this;
    }
    /**
     * Gets messenger
     *
     * @return \Data2CRMAPI\Model\Messenger[]
     */
    public function getMessenger()
    {
        return $this->offsetGet('messenger');
    }

    /**
     * Sets messenger
     *
     * @param \Data2CRMAPI\Model\Messenger[] $messenger Messenger
     *
     * @return $this
     */
    public function setMessenger($messenger)
    {
        $this->offsetSet('messenger', $messenger);

        return $this;
    }
    /**
     * Gets website
     *
     * @return \Data2CRMAPI\Model\Website[]
     */
    public function getWebsite()
    {
        return $this->offsetGet('website');
    }

    /**
     * Sets website
     *
     * @param \Data2CRMAPI\Model\Website[] $website Website
     *
     * @return $this
     */
    public function setWebsite($website)
    {
        $this->offsetSet('website', $website);

        return $this;
    }
    /**
     * Gets address
     *
     * @return \Data2CRMAPI\Model\Address[]
     */
    public function getAddress()
    {
        return $this->offsetGet('address');
    }

    /**
     * Sets address
     *
     * @param \Data2CRMAPI\Model\Address[] $address Address
     *
     * @return $this
     */
    public function setAddress($address)
    {
        $this->offsetSet('address', $address);

        return $this;
    }
    /**
     * Gets parent
     *
     * @return \Data2CRMAPI\Model\LeadEntityRelation
     */
    public function getParent()
    {
        return $this->offsetGet('parent');
    }

    /**
     * Sets parent
     *
     * @param \Data2CRMAPI\Model\LeadEntityRelation $parent Parent
     *
     * @return $this
     */
    public function setParent($parent)
    {
        $this->offsetSet('parent', $parent);

        return $this;
    }
    /**
     * Gets relation
     *
     * @return \Data2CRMAPI\Model\Relation
     */
    public function getRelation()
    {
        return $this->offsetGet('relation');
    }

    /**
     * Sets relation
     *
     * @param \Data2CRMAPI\Model\Relation $relation Relation
     *
     * @return $this
     */
    public function setRelation($relation)
    {
        $this->offsetSet('relation', $relation);

        return $this;
    }
    /**
     * Gets assignedUser
     *
     * @return \Data2CRMAPI\Model\UserEntityRelation
     */
    public function getAssignedUser()
    {
        return $this->offsetGet('assignedUser');
    }

    /**
     * Sets assignedUser
     *
     * @param \Data2CRMAPI\Model\UserEntityRelation $assignedUser Assigned User
     *
     * @return $this
     */
    public function setAssignedUser($assignedUser)
    {
        $this->offsetSet('assignedUser', $assignedUser);

        return $this;
    }
    /**
     * Gets user
     *
     * @return \Data2CRMAPI\Model\UserEntityRelation
     */
    public function getUser()
    {
        return $this->offsetGet('user');
    }

    /**
     * Sets user
     *
     * @param \Data2CRMAPI\Model\UserEntityRelation $user User
     *
     * @return $this
     */
    public function setUser($user)
    {
        $this->offsetSet('user', $user);

        return $this;
    }
    /**
     * Gets createdBy
     *
     * @return \Data2CRMAPI\Model\UserEntityRelation
     */
    public function getCreatedBy()
    {
        return $this->offsetGet('createdBy');
    }

    /**
     * Sets createdBy
     *
     * @param \Data2CRMAPI\Model\UserEntityRelation $createdBy Created By
     *
     * @return $this
     */
    public function setCreatedBy($createdBy)
    {
        $this->offsetSet('createdBy', $createdBy);

        return $this;
    }
    /**
     * Gets createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->offsetGet('createdAt');
    }

    /**
     * Sets createdAt
     *
     * @param \DateTime $createdAt Created At
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->offsetSet('createdAt', $createdAt);

        return $this;
    }
    /**
     * Gets updatedBy
     *
     * @return \Data2CRMAPI\Model\UserEntityRelation
     */
    public function getUpdatedBy()
    {
        return $this->offsetGet('updatedBy');
    }

    /**
     * Sets updatedBy
     *
     * @param \Data2CRMAPI\Model\UserEntityRelation $updatedBy Updated By
     *
     * @return $this
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->offsetSet('updatedBy', $updatedBy);

        return $this;
    }
    /**
     * Gets updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->offsetGet('updatedAt');
    }

    /**
     * Sets updatedAt
     *
     * @param \DateTime $updatedAt Updated At
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->offsetSet('updatedAt', $updatedAt);

        return $this;
    }
}
