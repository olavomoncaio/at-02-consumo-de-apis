<?php

namespace Data2CRMAPI\Model;

use \ArrayAccess;

class MeetingDescribe extends AbstractModel  implements ArrayAccess
{   
    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'entity' => 'string',
        'schema' => '\Data2CRMAPI\Model\SchemaDescribe'
    );

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = array(
        'entity' => 'entity',
        'schema' => 'schema'
    );

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = array(
        'entity' => 'setEntity',
        'schema' => 'setSchema'
    );

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = array(
        'entity' => 'getEntity',
        'schema' => 'getSchema'
    );

    /**
     * Gets entity
     *
     * @return string
     */
    public function getEntity()
    {
        return $this->offsetGet('entity');
    }

    /**
     * Sets entity
     *
     * @param string $entity Entity
     *
     * @return $this
     */
    public function setEntity($entity)
    {
        $this->offsetSet('entity', $entity);

        return $this;
    }
    /**
     * Gets schema
     *
     * @return \Data2CRMAPI\Model\SchemaDescribe
     */
    public function getSchema()
    {
        return $this->offsetGet('schema');
    }

    /**
     * Sets schema
     *
     * @param \Data2CRMAPI\Model\SchemaDescribe $schema Schema
     *
     * @return $this
     */
    public function setSchema($schema)
    {
        $this->offsetSet('schema', $schema);

        return $this;
    }
}
