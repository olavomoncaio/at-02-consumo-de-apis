<?php

namespace Data2CRMAPI\Model;

use \ArrayAccess;
use Data2CRMAPI\ApiException;

abstract class AbstractModel implements ArrayAccess
{
    /**
     * @var array
     */
    protected static $swaggerTypes = [];

    /**
     * @var array
     */
    protected static $attributeMap = [];

    /**
     * @var array
     */
    protected static $setters = [];

    /**
     * @var array
     */
    protected static $getters = [];

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = array();

    /**
     * @var array
     */
    protected $raw = array();

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property value initializing the model
     */
    public function __construct(array $data = array())
    {
        foreach (array_keys(static::$attributeMap) as $key) {
            $this->offsetSet($key, isset($data[$key]) ? $data[$key] : null);
        }
    }

    /**
     * @return string[]
     */
    public static function swaggerTypes()
    {
        return static::$swaggerTypes;
    }

    /**
     * @return string[]
     */
    public static function attributeMap()
    {
        return static::$attributeMap;
    }

    /**
     * @return string[]
     */
    public static function setters()
    {
        return static::$setters;
    }

    /**
     * @return string[]
     */
    public static function getters()
    {
        return static::$getters;
    }

    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param  integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return true === $this->offsetExists($offset) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset
     *
     * @param integer $offset Offset
     * @param mixed $value Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        $this->container[$offset] = $value;
    }

    /**
     * Unsets offset
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * @param mixed $valuesOrValue
     * @param null|string $key
     *
     * @return $this
     *
     * @throws ApiException
     */
    public function setRaw($valuesOrValue, $key = null)
    {
        if (null !== $key) {
            $this->raw[$key] = $valuesOrValue;
        } else if (true === is_array($valuesOrValue)) {
            $this->raw = $valuesOrValue;
        } else {
            throw new ApiException('Invalid arguments');
        }

        return $this;
    }

    /**
     * @param null|string $key
     * @param null|string $default
     *
     * @return array
     */
    public function getRaw($key = null, $default = null)
    {
        if (null !== $key) {
            if (true === isset($this->raw[$key])) {
                return $this->raw[$key];
            }

            return $default;
        }

        return $this->raw;
    }
}