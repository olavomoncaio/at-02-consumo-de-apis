<?php

namespace Data2CRMAPI\Model;

use \ArrayAccess;

class OpportunityEntity extends AbstractModel  implements ArrayAccess
{   
    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'id' => 'string',
        'name' => 'string',
        'description' => 'string',
        'type' => 'string',
        'leadSource' => 'string',
        'currency' => '\Data2CRMAPI\Model\Currency',
        'expectedAmount' => 'float',
        'amount' => 'float',
        'state' => 'string',
        'pipeline' => 'string',
        'stage' => 'string',
        'nextStep' => 'string',
        'probability' => 'int',
        'expectedEndAt' => '\DateTime',
        'endedAt' => '\DateTime',
        'parent' => '\Data2CRMAPI\Model\OpportunityEntityRelation',
        'relation' => '\Data2CRMAPI\Model\Relation',
        'assignedUser' => '\Data2CRMAPI\Model\UserEntityRelation',
        'user' => '\Data2CRMAPI\Model\UserEntityRelation',
        'createdBy' => '\Data2CRMAPI\Model\UserEntityRelation',
        'createdAt' => '\DateTime',
        'updatedBy' => '\Data2CRMAPI\Model\UserEntityRelation',
        'updatedAt' => '\DateTime'
    );

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = array(
        'id' => 'id',
        'name' => 'name',
        'description' => 'description',
        'type' => 'type',
        'leadSource' => 'lead_source',
        'currency' => 'currency',
        'expectedAmount' => 'expected_amount',
        'amount' => 'amount',
        'state' => 'state',
        'pipeline' => 'pipeline',
        'stage' => 'stage',
        'nextStep' => 'next_step',
        'probability' => 'probability',
        'expectedEndAt' => 'expected_end_at',
        'endedAt' => 'ended_at',
        'parent' => 'parent',
        'relation' => 'relation',
        'assignedUser' => 'assigned_user',
        'user' => 'user',
        'createdBy' => 'created_by',
        'createdAt' => 'created_at',
        'updatedBy' => 'updated_by',
        'updatedAt' => 'updated_at'
    );

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = array(
        'id' => 'setId',
        'name' => 'setName',
        'description' => 'setDescription',
        'type' => 'setType',
        'leadSource' => 'setLeadSource',
        'currency' => 'setCurrency',
        'expectedAmount' => 'setExpectedAmount',
        'amount' => 'setAmount',
        'state' => 'setState',
        'pipeline' => 'setPipeline',
        'stage' => 'setStage',
        'nextStep' => 'setNextStep',
        'probability' => 'setProbability',
        'expectedEndAt' => 'setExpectedEndAt',
        'endedAt' => 'setEndedAt',
        'parent' => 'setParent',
        'relation' => 'setRelation',
        'assignedUser' => 'setAssignedUser',
        'user' => 'setUser',
        'createdBy' => 'setCreatedBy',
        'createdAt' => 'setCreatedAt',
        'updatedBy' => 'setUpdatedBy',
        'updatedAt' => 'setUpdatedAt'
    );

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = array(
        'id' => 'getId',
        'name' => 'getName',
        'description' => 'getDescription',
        'type' => 'getType',
        'leadSource' => 'getLeadSource',
        'currency' => 'getCurrency',
        'expectedAmount' => 'getExpectedAmount',
        'amount' => 'getAmount',
        'state' => 'getState',
        'pipeline' => 'getPipeline',
        'stage' => 'getStage',
        'nextStep' => 'getNextStep',
        'probability' => 'getProbability',
        'expectedEndAt' => 'getExpectedEndAt',
        'endedAt' => 'getEndedAt',
        'parent' => 'getParent',
        'relation' => 'getRelation',
        'assignedUser' => 'getAssignedUser',
        'user' => 'getUser',
        'createdBy' => 'getCreatedBy',
        'createdAt' => 'getCreatedAt',
        'updatedBy' => 'getUpdatedBy',
        'updatedAt' => 'getUpdatedAt'
    );

    /**
     * Gets id
     *
     * @return string
     */
    public function getId()
    {
        return $this->offsetGet('id');
    }

    /**
     * Sets id
     *
     * @param string $id Opportunity Identifier
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->offsetSet('id', $id);

        return $this;
    }
    /**
     * Gets name
     *
     * @return string
     */
    public function getName()
    {
        return $this->offsetGet('name');
    }

    /**
     * Sets name
     *
     * @param string $name Name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->offsetSet('name', $name);

        return $this;
    }
    /**
     * Gets description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->offsetGet('description');
    }

    /**
     * Sets description
     *
     * @param string $description Description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->offsetSet('description', $description);

        return $this;
    }
    /**
     * Gets type
     *
     * @return string
     */
    public function getType()
    {
        return $this->offsetGet('type');
    }

    /**
     * Sets type
     *
     * @param string $type Type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->offsetSet('type', $type);

        return $this;
    }
    /**
     * Gets leadSource
     *
     * @return string
     */
    public function getLeadSource()
    {
        return $this->offsetGet('leadSource');
    }

    /**
     * Sets leadSource
     *
     * @param string $leadSource Lead Source
     *
     * @return $this
     */
    public function setLeadSource($leadSource)
    {
        $this->offsetSet('leadSource', $leadSource);

        return $this;
    }
    /**
     * Gets currency
     *
     * @return \Data2CRMAPI\Model\Currency
     */
    public function getCurrency()
    {
        return $this->offsetGet('currency');
    }

    /**
     * Sets currency
     *
     * @param \Data2CRMAPI\Model\Currency $currency Currency
     *
     * @return $this
     */
    public function setCurrency($currency)
    {
        $this->offsetSet('currency', $currency);

        return $this;
    }
    /**
     * Gets expectedAmount
     *
     * @return float
     */
    public function getExpectedAmount()
    {
        return $this->offsetGet('expectedAmount');
    }

    /**
     * Sets expectedAmount
     *
     * @param float $expectedAmount Expected Amount
     *
     * @return $this
     */
    public function setExpectedAmount($expectedAmount)
    {
        $this->offsetSet('expectedAmount', $expectedAmount);

        return $this;
    }
    /**
     * Gets amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->offsetGet('amount');
    }

    /**
     * Sets amount
     *
     * @param float $amount Amount
     *
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->offsetSet('amount', $amount);

        return $this;
    }
    /**
     * Gets state
     *
     * @return string
     */
    public function getState()
    {
        return $this->offsetGet('state');
    }

    /**
     * Sets state
     *
     * @param string $state State
     *
     * @return $this
     */
    public function setState($state)
    {
        $this->offsetSet('state', $state);

        return $this;
    }
    /**
     * Gets pipeline
     *
     * @return string
     */
    public function getPipeline()
    {
        return $this->offsetGet('pipeline');
    }

    /**
     * Sets pipeline
     *
     * @param string $pipeline Pipeline
     *
     * @return $this
     */
    public function setPipeline($pipeline)
    {
        $this->offsetSet('pipeline', $pipeline);

        return $this;
    }
    /**
     * Gets stage
     *
     * @return string
     */
    public function getStage()
    {
        return $this->offsetGet('stage');
    }

    /**
     * Sets stage
     *
     * @param string $stage Stage
     *
     * @return $this
     */
    public function setStage($stage)
    {
        $this->offsetSet('stage', $stage);

        return $this;
    }
    /**
     * Gets nextStep
     *
     * @return string
     */
    public function getNextStep()
    {
        return $this->offsetGet('nextStep');
    }

    /**
     * Sets nextStep
     *
     * @param string $nextStep Next Step
     *
     * @return $this
     */
    public function setNextStep($nextStep)
    {
        $this->offsetSet('nextStep', $nextStep);

        return $this;
    }
    /**
     * Gets probability
     *
     * @return int
     */
    public function getProbability()
    {
        return $this->offsetGet('probability');
    }

    /**
     * Sets probability
     *
     * @param int $probability Probability
     *
     * @return $this
     */
    public function setProbability($probability)
    {
        $this->offsetSet('probability', $probability);

        return $this;
    }
    /**
     * Gets expectedEndAt
     *
     * @return \DateTime
     */
    public function getExpectedEndAt()
    {
        return $this->offsetGet('expectedEndAt');
    }

    /**
     * Sets expectedEndAt
     *
     * @param \DateTime $expectedEndAt Expected End At
     *
     * @return $this
     */
    public function setExpectedEndAt($expectedEndAt)
    {
        $this->offsetSet('expectedEndAt', $expectedEndAt);

        return $this;
    }
    /**
     * Gets endedAt
     *
     * @return \DateTime
     */
    public function getEndedAt()
    {
        return $this->offsetGet('endedAt');
    }

    /**
     * Sets endedAt
     *
     * @param \DateTime $endedAt Closed At
     *
     * @return $this
     */
    public function setEndedAt($endedAt)
    {
        $this->offsetSet('endedAt', $endedAt);

        return $this;
    }
    /**
     * Gets parent
     *
     * @return \Data2CRMAPI\Model\OpportunityEntityRelation
     */
    public function getParent()
    {
        return $this->offsetGet('parent');
    }

    /**
     * Sets parent
     *
     * @param \Data2CRMAPI\Model\OpportunityEntityRelation $parent Parent
     *
     * @return $this
     */
    public function setParent($parent)
    {
        $this->offsetSet('parent', $parent);

        return $this;
    }
    /**
     * Gets relation
     *
     * @return \Data2CRMAPI\Model\Relation
     */
    public function getRelation()
    {
        return $this->offsetGet('relation');
    }

    /**
     * Sets relation
     *
     * @param \Data2CRMAPI\Model\Relation $relation Relation
     *
     * @return $this
     */
    public function setRelation($relation)
    {
        $this->offsetSet('relation', $relation);

        return $this;
    }
    /**
     * Gets assignedUser
     *
     * @return \Data2CRMAPI\Model\UserEntityRelation
     */
    public function getAssignedUser()
    {
        return $this->offsetGet('assignedUser');
    }

    /**
     * Sets assignedUser
     *
     * @param \Data2CRMAPI\Model\UserEntityRelation $assignedUser Assigned User
     *
     * @return $this
     */
    public function setAssignedUser($assignedUser)
    {
        $this->offsetSet('assignedUser', $assignedUser);

        return $this;
    }
    /**
     * Gets user
     *
     * @return \Data2CRMAPI\Model\UserEntityRelation
     */
    public function getUser()
    {
        return $this->offsetGet('user');
    }

    /**
     * Sets user
     *
     * @param \Data2CRMAPI\Model\UserEntityRelation $user User
     *
     * @return $this
     */
    public function setUser($user)
    {
        $this->offsetSet('user', $user);

        return $this;
    }
    /**
     * Gets createdBy
     *
     * @return \Data2CRMAPI\Model\UserEntityRelation
     */
    public function getCreatedBy()
    {
        return $this->offsetGet('createdBy');
    }

    /**
     * Sets createdBy
     *
     * @param \Data2CRMAPI\Model\UserEntityRelation $createdBy Created By
     *
     * @return $this
     */
    public function setCreatedBy($createdBy)
    {
        $this->offsetSet('createdBy', $createdBy);

        return $this;
    }
    /**
     * Gets createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->offsetGet('createdAt');
    }

    /**
     * Sets createdAt
     *
     * @param \DateTime $createdAt Created At
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->offsetSet('createdAt', $createdAt);

        return $this;
    }
    /**
     * Gets updatedBy
     *
     * @return \Data2CRMAPI\Model\UserEntityRelation
     */
    public function getUpdatedBy()
    {
        return $this->offsetGet('updatedBy');
    }

    /**
     * Sets updatedBy
     *
     * @param \Data2CRMAPI\Model\UserEntityRelation $updatedBy Updated By
     *
     * @return $this
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->offsetSet('updatedBy', $updatedBy);

        return $this;
    }
    /**
     * Gets updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->offsetGet('updatedAt');
    }

    /**
     * Sets updatedAt
     *
     * @param \DateTime $updatedAt Updated At
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->offsetSet('updatedAt', $updatedAt);

        return $this;
    }
}
