<?php

namespace Data2CRMAPI;

class ApiClient
{
    /**
     * Configuration
     *
     * @var Configuration
     */
    protected $configuration;

    /**
     * Object Serializer
     *
     * @var ObjectSerializer
     */
    protected $serializer;

    /**
     * Constructor of the class
     *
     * @param Configuration $configuration config for this ApiClient
     */
    public function __construct(Configuration $configuration)
    {
        $this->configuration = $configuration;
        $this->serializer = new ObjectSerializer();
    }

    /**
     * Get the config
     *
     * @return Configuration
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }

    /**
     * Get the serializer
     *
     * @return ObjectSerializer
     */
    public function getSerializer()
    {
        return $this->serializer;
    }

    /**
     * Make the HTTP call (Sync)
     *
     * @param string $path         path to method endpoint
     * @param string $method       method to call
     * @param array  $query        parameters to be place in query URL
     * @param array  $post         parameters to be placed in POST body
     * @param array  $headers      parameters to be place in request header
     * @param string $responseType expected response type of the endpoint
     * @param Data   $data         data
     *
     * @return mixed
     *
     * @throws ApiException on a non 2xx response
     */
    public function call($path, $method, $query, $post, $headers, $responseType, Data $data)
    {
        $data->reset();

        list ($url, $info, $headers, $body) = $this->doRequest($path, $method, $query, $post, $headers);

        return $this->readResponse($method, $url, $info, $headers, $body, $responseType, $data);
    }

    /**
     * @param string $path         path to method endpoint
     * @param string $method       method to call
     * @param array  $query        parameters to be place in query URL
     * @param array  $post         parameters to be placed in POST body
     * @param array  $headers      parameters to be place in request header
     * 
     * @return array
     * 
     * @throws ApiException
     */
    protected function doRequest($path, $method, $query, $post, $headers)
    {
        $headersCompiled = array();
        foreach (array_merge((array)$this->configuration->getDefaultHeaders(), (array)$headers) as $key => $val) {
            $headersCompiled[] = "$key: $val";
        }

        $url = $this->configuration->getHost() . $path;

        $curl = curl_init();

        if (0 !== $this->configuration->getTimeout()) {
            curl_setopt($curl, CURLOPT_TIMEOUT, $this->configuration->getTimeout());
        }
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headersCompiled);

        // disable SSL verification, if needed
        if ($this->configuration->getSSLVerification() == false) {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        }

        if (!empty($query)) {
            $url .= '?' . http_build_query($query);
        }

        if ($method == 'POST') {
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode(ObjectSerializer::sanitizeForSerialization($post)));
        } else if ($method == 'PUT') {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode(ObjectSerializer::sanitizeForSerialization($post)));
        } else if ($method == 'DELETE') {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
        } else if ($method = 'GET') {

        } else {
            throw new ApiException('Method ' . $method . ' is not recognized.');
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_USERAGENT, $this->configuration->getUserAgent());
        curl_setopt($curl, CURLOPT_VERBOSE, 0);
        curl_setopt($curl, CURLOPT_HEADER, 1);

        // Make the request
        $response = curl_exec($curl);
        $httpHeaderSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);

        return [
            $url, // url
            curl_getinfo($curl), // info
            $this->httpParseHeaders(substr($response, 0, $httpHeaderSize)), // headers
            substr($response, $httpHeaderSize) // body
        ];
    }

    /**
     * @param string $method       method to call
     * @param string $url          URL
     * @param array  $info         request-response information
     * @param array  $headers      response headers
     * @param string $body         response body
     * @param string $responseType expected response type of the endpoint
     * @param Data   $data         data
     * 
     * @return array
     * 
     * @throws ApiException
     */
    protected function readResponse($method, $url, array $info, array $headers, $body, $responseType, Data $data)
    {
        switch ($info['http_code']) {

            case 0:
                throw new ApiException('API call to ' . $url . ' timed out: ' . json_encode($info));

            case 200:
                if ('GET' === $method) {
                    if (true === isset($headers[$data::HEADER_ENABLE])) {
                        $data->setEnable($data::ENABLE_TRUE === $headers[$data::HEADER_ENABLE]);
                        if (true === $data->isEnabled()) {
                            $data->setStatus($headers[$data::HEADER_STATUS]);
                            $data->setStatusMessage($headers[$data::HEADER_STATUS_MESSAGE]);
                        }
                    }

                    $check = json_decode($body, true);
                    if (true === is_array($check) && true === empty($check)) {
                        return array();
                    }
                }

                if ('GET' === $method || 'PUT' === $method) {
                    return $this->getSerializer()->deserialize(json_decode($body), $responseType, $headers);
                }
                break;

            case 201:
                if ('POST' === $method) {
                    return $this->getSerializer()->deserialize(json_decode($body), $responseType, $headers);
                }
                break;

            case 202:
                $data->setStatus($headers[$data::HEADER_STATUS]);
                $data->setStatusMessage($headers[$data::HEADER_STATUS_MESSAGE]);
                return null;

            case 204:
                if ('DELETE' === $method) {
                    return true;
                }
                break;

            case 400:
            case 401:
            case 403:
            case 429:
            case 500:
                throw ApiException::fromResponseBody(json_decode($body, true));

        }

        throw new ApiException('Something wrong API call to ' . $url . ': ' . json_encode($info));
    }

   /**
    * Return an array of HTTP response headers
    *
    * @param string $rawHeaders A string of raw HTTP response headers
    *
    * @return string[] Array of HTTP response heaers
    */
    protected function httpParseHeaders($rawHeaders)
    {
        // ref/credit: http://php.net/manual/en/function.http-parse-headers.php#112986
        $headers = array();
        $key = '';

        foreach (explode("\n", $rawHeaders) as $h) {
            $h = explode(':', $h, 2);

            if (isset($h[1])) {
                if (!isset($headers[$h[0]])) {
                    $headers[$h[0]] = trim($h[1]);
                } elseif (is_array($headers[$h[0]])) {
                    $headers[$h[0]] = array_merge($headers[$h[0]], array(trim($h[1])));
                } else {
                    $headers[$h[0]] = array_merge(array($headers[$h[0]]), array(trim($h[1])));
                }

                $key = $h[0];
            } else {
                if (substr($h[0], 0, 1) == "\t") {
                    $headers[$key] .= "\r\n\t".trim($h[0]);
                } elseif (!$key) {
                    $headers[0] = trim($h[0]);
                }
                trim($h[0]);
            }
        }

        return $headers;
    }
}
?>
<h1> oi </h1>
