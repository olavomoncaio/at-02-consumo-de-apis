<?php

namespace Data2CRMAPI\Resource;

use Data2CRMAPI\Model\UserDescribe;
use Data2CRMAPI\Model\UserEntity;
use Data2CRMAPI\Model\UserEntityRelation;
use Data2CRMAPI\Model\Count;

class UserApi extends AbstractApi
{
    const HAS_QUERY_FILTER = true;
    const HAS_QUERY_FIELDS = true;
    
    /**
     * @var string
     */
    protected $path = '/user';

    /**
     * @return UserDescribe
     */
    public function describe()
    {
        return $this->doDescribe('\Data2CRMAPI\Model\UserDescribe');
    }

    /**
     * @return Count
     */
    public function count()
    {
        return $this->doCount('\Data2CRMAPI\Model\Count');
    }

    /**
     * @param null|int $pageSize
     * @param null|int $page
     * @param array $filter
     * @param array $fields
     * 
     * @return UserEntity[]
     */
    public function fetchAll($pageSize = null, $page = null, array $filter = array(), array $fields = array())
    {
        return $this->doFetchAll($pageSize, $page, $filter, $fields, '\Data2CRMAPI\Model\UserEntity[]');
    }

    /**
     * @param string $id
     * 
     * @return UserEntity
     */
    public function fetch($id)
    {
        return $this->doFetch($id, '\Data2CRMAPI\Model\UserEntity');
    }

    /**
     * @param UserEntity $user
     *
     * @return UserEntityRelation
     */
    public function create(UserEntity $user)
    {
        return $this->doCreate($user, '\Data2CRMAPI\Model\UserEntityRelation');
    }

    /**
     * @param string $id
     * @param UserEntity $user
     * 
     * @return UserEntityRelation
     */
    public function update($id, UserEntity $user)
    {
        return parent::doUpdate($id, $user, '\Data2CRMAPI\Model\UserEntityRelation');
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        parent::doDelete($id);
    }
}
