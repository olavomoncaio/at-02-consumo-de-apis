<?php

namespace Data2CRMAPI\Resource;

use Data2CRMAPI\Model\OpportunityDescribe;
use Data2CRMAPI\Model\OpportunityEntity;
use Data2CRMAPI\Model\OpportunityEntityRelation;
use Data2CRMAPI\Model\Count;

class OpportunityApi extends AbstractApi
{
    const HAS_QUERY_FILTER = true;
    const HAS_QUERY_FIELDS = true;
    
    /**
     * @var string
     */
    protected $path = '/opportunity';

    /**
     * @return OpportunityDescribe
     */
    public function describe()
    {
        return $this->doDescribe('\Data2CRMAPI\Model\OpportunityDescribe');
    }

    /**
     * @return Count
     */
    public function count()
    {
        return $this->doCount('\Data2CRMAPI\Model\Count');
    }

    /**
     * @param null|int $pageSize
     * @param null|int $page
     * @param array $filter
     * @param array $fields
     * 
     * @return OpportunityEntity[]
     */
    public function fetchAll($pageSize = null, $page = null, array $filter = array(), array $fields = array())
    {
        return $this->doFetchAll($pageSize, $page, $filter, $fields, '\Data2CRMAPI\Model\OpportunityEntity[]');
    }

    /**
     * @param string $id
     * 
     * @return OpportunityEntity
     */
    public function fetch($id)
    {
        return $this->doFetch($id, '\Data2CRMAPI\Model\OpportunityEntity');
    }

    /**
     * @param OpportunityEntity $opportunity
     *
     * @return OpportunityEntityRelation
     */
    public function create(OpportunityEntity $opportunity)
    {
        return $this->doCreate($opportunity, '\Data2CRMAPI\Model\OpportunityEntityRelation');
    }

    /**
     * @param string $id
     * @param OpportunityEntity $opportunity
     * 
     * @return OpportunityEntityRelation
     */
    public function update($id, OpportunityEntity $opportunity)
    {
        return parent::doUpdate($id, $opportunity, '\Data2CRMAPI\Model\OpportunityEntityRelation');
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        parent::doDelete($id);
    }
}
