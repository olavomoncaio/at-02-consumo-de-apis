<?php

namespace Data2CRMAPI\Resource;

use Data2CRMAPI\Model\ContactDescribe;
use Data2CRMAPI\Model\ContactEntity;
use Data2CRMAPI\Model\ContactEntityRelation;
use Data2CRMAPI\Model\Count;

class ContactApi extends AbstractApi
{
    const HAS_QUERY_FILTER = true;
    const HAS_QUERY_FIELDS = true;
    
    /**
     * @var string
     */
    protected $path = '/contact';

    /**
     * @return ContactDescribe
     */
    public function describe()
    {
        return $this->doDescribe('\Data2CRMAPI\Model\ContactDescribe');
    }

    /**
     * @return Count
     */
    public function count()
    {
        return $this->doCount('\Data2CRMAPI\Model\Count');
    }

    /**
     * @param null|int $pageSize
     * @param null|int $page
     * @param array $filter
     * @param array $fields
     * 
     * @return ContactEntity[]
     */
    public function fetchAll($pageSize = null, $page = null, array $filter = array(), array $fields = array())
    {
        return $this->doFetchAll($pageSize, $page, $filter, $fields, '\Data2CRMAPI\Model\ContactEntity[]');
    }

    /**
     * @param string $id
     * 
     * @return ContactEntity
     */
    public function fetch($id)
    {
        return $this->doFetch($id, '\Data2CRMAPI\Model\ContactEntity');
    }

    /**
     * @param ContactEntity $contact
     *
     * @return ContactEntityRelation
     */
    public function create(ContactEntity $contact)
    {
        return $this->doCreate($contact, '\Data2CRMAPI\Model\ContactEntityRelation');
    }

    /**
     * @param string $id
     * @param ContactEntity $contact
     * 
     * @return ContactEntityRelation
     */
    public function update($id, ContactEntity $contact)
    {
        return parent::doUpdate($id, $contact, '\Data2CRMAPI\Model\ContactEntityRelation');
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        parent::doDelete($id);
    }
}
