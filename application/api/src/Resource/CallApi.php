<?php

namespace Data2CRMAPI\Resource;

use Data2CRMAPI\Model\CallDescribe;
use Data2CRMAPI\Model\CallEntity;
use Data2CRMAPI\Model\CallEntityRelation;
use Data2CRMAPI\Model\Count;

class CallApi extends AbstractApi
{
    const HAS_QUERY_FILTER = true;
    const HAS_QUERY_FIELDS = true;
    
    /**
     * @var string
     */
    protected $path = '/call';

    /**
     * @return CallDescribe
     */
    public function describe()
    {
        return $this->doDescribe('\Data2CRMAPI\Model\CallDescribe');
    }

    /**
     * @return Count
     */
    public function count()
    {
        return $this->doCount('\Data2CRMAPI\Model\Count');
    }

    /**
     * @param null|int $pageSize
     * @param null|int $page
     * @param array $filter
     * @param array $fields
     * 
     * @return CallEntity[]
     */
    public function fetchAll($pageSize = null, $page = null, array $filter = array(), array $fields = array())
    {
        return $this->doFetchAll($pageSize, $page, $filter, $fields, '\Data2CRMAPI\Model\CallEntity[]');
    }

    /**
     * @param string $id
     * 
     * @return CallEntity
     */
    public function fetch($id)
    {
        return $this->doFetch($id, '\Data2CRMAPI\Model\CallEntity');
    }

    /**
     * @param CallEntity $call
     *
     * @return CallEntityRelation
     */
    public function create(CallEntity $call)
    {
        return $this->doCreate($call, '\Data2CRMAPI\Model\CallEntityRelation');
    }

    /**
     * @param string $id
     * @param CallEntity $Call
     * 
     * @return CallEntityRelation
     */
    public function update($id, CallEntity $call)
    {
        return parent::doUpdate($id, $call, '\Data2CRMAPI\Model\CallEntityRelation');
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        parent::doDelete($id);
    }
}
