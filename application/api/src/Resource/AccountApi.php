<?php

namespace Data2CRMAPI\Resource;

use Data2CRMAPI\Model\AccountDescribe;
use Data2CRMAPI\Model\AccountEntity;
use Data2CRMAPI\Model\AccountEntityRelation;
use Data2CRMAPI\Model\Count;

class AccountApi extends AbstractApi
{
    const HAS_QUERY_FILTER = true;
    const HAS_QUERY_FIELDS = true;
    
    /**
     * @var string
     */
    protected $path = '/account';

    /**
     * @return AccountDescribe
     */
    public function describe()
    {
        return $this->doDescribe('\Data2CRMAPI\Model\AccountDescribe');
    }

    /**
     * @return Count
     */
    public function count()
    {
        return $this->doCount('\Data2CRMAPI\Model\Count');
    }

    /**
     * @param null|int $pageSize
     * @param null|int $page
     * @param array $filter
     * @param array $fields
     * 
     * @return AccountEntity[]
     */
    public function fetchAll($pageSize = null, $page = null, array $filter = array(), array $fields = array())
    {
        return $this->doFetchAll($pageSize, $page, $filter, $fields, '\Data2CRMAPI\Model\AccountEntity[]');
    }

    /**
     * @param string $id
     * 
     * @return AccountEntity
     */
    public function fetch($id)
    {
        return $this->doFetch($id, '\Data2CRMAPI\Model\AccountEntity');
    }

    /**
     * @param AccountEntity $account
     *
     * @return AccountEntityRelation
     */
    public function create(AccountEntity $account)
    {
        return $this->doCreate($account, '\Data2CRMAPI\Model\AccountEntityRelation');
    }

    /**
     * @param string $id
     * @param AccountEntity $account
     * 
     * @return AccountEntityRelation
     */
    public function update($id, AccountEntity $account)
    {
        return parent::doUpdate($id, $account, '\Data2CRMAPI\Model\AccountEntityRelation');
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        parent::doDelete($id);
    }
}
