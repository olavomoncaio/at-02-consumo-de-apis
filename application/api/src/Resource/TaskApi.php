<?php

namespace Data2CRMAPI\Resource;

use Data2CRMAPI\Model\TaskDescribe;
use Data2CRMAPI\Model\TaskEntity;
use Data2CRMAPI\Model\TaskEntityRelation;
use Data2CRMAPI\Model\Count;

class TaskApi extends AbstractApi
{
    const HAS_QUERY_FILTER = true;
    const HAS_QUERY_FIELDS = true;

    /**
     * @var string
     */
    protected $path = '/task';

    /**
     * @return TaskDescribe
     */
    public function describe()
    {
        return $this->doDescribe('\Data2CRMAPI\Model\TaskDescribe');
    }

    /**
     * @return Count
     */
    public function count()
    {
        return $this->doCount('\Data2CRMAPI\Model\Count');
    }

    /**
     * @param null|int $pageSize
     * @param null|int $page
     * @param array $filter
     * @param array $fields
     *
     * @return TaskEntity[]
     */
    public function fetchAll($pageSize = null, $page = null, array $filter = array(), array $fields = array())
    {
        return $this->doFetchAll($pageSize, $page, $filter, $fields, '\Data2CRMAPI\Model\TaskEntity[]');
    }

    /**
     * @param string $id
     *
     * @return TaskEntity
     */
    public function fetch($id)
    {
        return $this->doFetch($id, '\Data2CRMAPI\Model\TaskEntity');
    }

    /**
     * @param TaskEntity $task
     *
     * @return TaskEntityRelation
     */
    public function create(TaskEntity $task)
    {
        return $this->doCreate($task, '\Data2CRMAPI\Model\TaskEntityRelation');
    }

    /**
     * @param string $id
     * @param TaskEntity $task
     *
     * @return TaskEntityRelation
     */
    public function update($id, TaskEntity $task)
    {
        return parent::doUpdate($id, $task, '\Data2CRMAPI\Model\TaskEntityRelation');
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        parent::doDelete($id);
    }
}