<?php

namespace Data2CRMAPI\Resource;

use Data2CRMAPI\Model\EmailDescribe;
use Data2CRMAPI\Model\EmailEntity;
use Data2CRMAPI\Model\EmailEntityRelation;
use Data2CRMAPI\Model\Count;

class EmailApi extends AbstractApi
{
    const HAS_QUERY_FILTER = true;
    const HAS_QUERY_FIELDS = true;
    
    /**
     * @var string
     */
    protected $path = '/email';

    /**
     * @return EmailDescribe
     */
    public function describe()
    {
        return $this->doDescribe('\Data2CRMAPI\Model\EmailDescribe');
    }

    /**
     * @return Count
     */
    public function count()
    {
        return $this->doCount('\Data2CRMAPI\Model\Count');
    }

    /**
     * @param null|int $pageSize
     * @param null|int $page
     * @param array $filter
     * @param array $fields
     * 
     * @return EmailEntity[]
     */
    public function fetchAll($pageSize = null, $page = null, array $filter = array(), array $fields = array())
    {
        return $this->doFetchAll($pageSize, $page, $filter, $fields, '\Data2CRMAPI\Model\EmailEntity[]');
    }

    /**
     * @param string $id
     * 
     * @return EmailEntity
     */
    public function fetch($id)
    {
        return $this->doFetch($id, '\Data2CRMAPI\Model\EmailEntity');
    }

    /**
     * @param EmailEntity $email
     *
     * @return EmailEntityRelation
     */
    public function create(EmailEntity $email)
    {
        return $this->doCreate($email, '\Data2CRMAPI\Model\EmailEntityRelation');
    }

    /**
     * @param string $id
     * @param EmailEntity $email
     * 
     * @return EmailEntityRelation
     */
    public function update($id, EmailEntity $email)
    {
        return parent::doUpdate($id, $email, '\Data2CRMAPI\Model\EmailEntityRelation');
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        parent::doDelete($id);
    }
}
