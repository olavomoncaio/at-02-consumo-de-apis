<?php

namespace Data2CRMAPI\Resource;

use Data2CRMAPI\Model\ApplicationEntity;
use Data2CRMAPI\Model\ApplicationEntityRelation;
use Data2CRMAPI\Model\ApplicationEntityWrite;
use Data2CRMAPI\Model\Count;

class ApplicationApi extends AbstractApi
{
    const HAS_QUERY_FILTER = false;
    const HAS_QUERY_FIELDS = true;
    
    /**
     * @var string
     */
    protected $path = '/application';
    
    /**
     * @return Count
     */
    public function count()
    {
        return $this->doCount('\Data2CRMAPI\Model\Count');
    }

    /**
     * @param null|int $pageSize
     * @param null|int $page
     * @param array $fields
     * 
     * @return ApplicationEntity[]
     */
    public function fetchAll($pageSize = null, $page = null, array $fields = array())
    {
        return $this->doFetchAll($pageSize, $page, [], $fields, '\Data2CRMAPI\Model\ApplicationEntity[]');
    }

    /**
     * @param string $id
     * 
     * @return ApplicationEntity
     */
    public function fetch($id)
    {
        return $this->doFetch($id, '\Data2CRMAPI\Model\ApplicationEntity');
    }

    /**
     * @param ApplicationEntityWrite $application
     *
     * @return ApplicationEntityRelation
     */
    public function create(ApplicationEntityWrite $application)
    {
        return $this->doCreate($application, '\Data2CRMAPI\Model\ApplicationEntityRelation');
    }

    /**
     * @param string $id
     * @param ApplicationEntityWrite $application
     * 
     * @return ApplicationEntityRelation
     */
    public function update($id, ApplicationEntityWrite $application)
    {
        return parent::doUpdate($id, $application, '\Data2CRMAPI\Model\ApplicationEntityRelation');
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        parent::doDelete($id);
    }
}
