<?php

namespace Data2CRMAPI\Resource;

use Data2CRMAPI\Model\CustomFieldEntity;
use Data2CRMAPI\Model\CustomFieldEntityRelation;

class CustomFieldApi extends AbstractApi
{
    const HAS_QUERY_FILTER = false;
    const HAS_QUERY_FIELDS = true;
    
    /**
     * @var string
     */
    protected $path = '/customField';

    /**
     * @param null|int $pageSize
     * @param null|int $page
     * @param array $fields
     * 
     * @return CustomFieldEntity[]
     */
    public function fetchAll($pageSize = null, $page = null, array $fields = array())
    {
        return $this->doFetchAll($pageSize, $page, [], $fields, '\Data2CRMAPI\Model\CustomFieldEntity[]');
    }

    /**
     * @param string $id
     * 
     * @return CustomFieldEntity
     */
    public function fetch($id)
    {
        return $this->doFetch($id, '\Data2CRMAPI\Model\CustomFieldEntity');
    }

    /**
     * @param CustomFieldEntity $customField
     *
     * @return CustomFieldEntityRelation
     */
    public function create(CustomFieldEntity $customField)
    {
        return $this->doCreate($customField, '\Data2CRMAPI\Model\CustomFieldEntityRelation');
    }

    /**
     * @param string $id
     * @param CustomFieldEntity $CustomField
     * 
     * @return CustomFieldEntityRelation
     */
    public function update($id, CustomFieldEntity $customField)
    {
        return parent::doUpdate($id, $customField, '\Data2CRMAPI\Model\CustomFieldEntityRelation');
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        parent::doDelete($id);
    }
}
