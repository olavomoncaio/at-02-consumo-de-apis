<?php

namespace Data2CRMAPI;

use \Exception;

class ApiException extends Exception
{
    /**
     * @var string
     */
    protected $type;
    
    /**
     * @var string
     */
    protected $detailCode;

    /**
     * @var string
     */
    protected $detailMessage;

    /**
     * @var array
     */
    protected $detailExtra = [];

    /**
     * @param array $body
     * 
     * @return ApiException
     */
    public static function fromResponseBody(array $body)
    {
        $exception = new ApiException($body['title'], (int)$body['status']);
        $exception->setType($body['type']);
        $exception->setDetailCode($body['detail']['code']);
        $exception->setDetailMessage($body['detail']['message']);
        $exception->setDetailExtra(true === isset($body['detail']['extra']) ? $body['detail']['extra'] : []);
        
        return $exception;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getDetailCode()
    {
        return $this->detailCode;
    }

    /**
     * @param string $detailCode
     */
    public function setDetailCode($detailCode)
    {
        $this->detailCode = $detailCode;
    }

    /**
     * @return string
     */
    public function getDetailMessage()
    {
        return $this->detailMessage;
    }

    /**
     * @param string $detailMessage
     */
    public function setDetailMessage($detailMessage)
    {
        $this->detailMessage = $detailMessage;
    }

    /**
     * @return array
     */
    public function getDetailExtra()
    {
        return $this->detailExtra;
    }

    /**
     * @param array $detailExtra
     */
    public function setDetailExtra($detailExtra)
    {
        $this->detailExtra = $detailExtra;
    }
}
