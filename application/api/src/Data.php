<?php

namespace Data2CRMAPI;

/**
 * Data
 *
 * @category API
 * @package  Data2CRMAPI
 */
class Data
{
    /**
     * Headers
     */
    const HEADER_ENABLE         = 'X-API2CRM-DATA-ENABLE';
    const HEADER_STATUS         = 'X-API2CRM-DATA-STATUS';
    const HEADER_STATUS_MESSAGE = 'X-API2CRM-DATA-STATUS-MESSAGE';
    
    /**
     * Enable
     */
    const ENABLE_TRUE   = 'true';
    const ENABLE_FALSE  = 'false';

    /**
     * Statuses
     */
    const STATUS_PREPARE    = 'prepare';
    const STATUS_NEW        = 'new';
    const STATUS_IN_PROCESS = 'in process';
    const STATUS_DONE       = 'done';
    const STATUS_FAILED     = 'failed';
    const STATUS_EXPIRED    = 'expired';
    
    /**
     * @var bool
     */
    protected $enable;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var string
     */
    protected $statusMessage;

    /**
     * Reset
     */
    public function reset()
    {
        $this->setEnable(static::ENABLE_FALSE);
        $this->setStatus(null);
        $this->setStatusMessage(null);
    }

    /**
     * @return bool
     */
    public function getEnable()
    {
        return $this->enable;
    }

    /**
     * @param bool $enable
     */
    public function setEnable($enable)
    {
        $this->enable = $enable;
    }

    /**
     * @return bool
     */
    public function enable()
    {
        $this->setEnable(static::ENABLE_TRUE);
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return true === $this->getEnable();
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return bool
     */
    public function isStatusPrepare()
    {
        return static::STATUS_PREPARE === $this->getStatus();
    }

    /**
     * @return bool
     */
    public function isStatusNew()
    {
        return static::STATUS_NEW === $this->getStatus();
    }

    /**
     * @return bool
     */
    public function isStatusInProgress()
    {
        return static::STATUS_IN_PROCESS === $this->getStatus();
    }

    /**
     * @return bool
     */
    public function isStatusDone()
    {
        return static::STATUS_DONE === $this->getStatus();
    }

    /**
     * @return bool
     */
    public function isStatusFailed()
    {
        return static::STATUS_FAILED === $this->getStatus();
    }

    /**
     * @return bool
     */
    public function isStatusExpired()
    {
        return static::STATUS_EXPIRED === $this->getStatus();
    }

    /**
     * @return string
     */
    public function getStatusMessage()
    {
        return $this->statusMessage;
    }

    /**
     * @param string $statusMessage
     */
    public function setStatusMessage($statusMessage)
    {
        $this->statusMessage = $statusMessage;
    }

    /**
     * @return bool
     */
    public function isDataUseAndStatusDone()
    {
        return true === $this->isEnabled() && true === $this->isStatusDone();
    }
}
