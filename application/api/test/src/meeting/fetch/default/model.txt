Data2CRMAPI\Model\MeetingEntity::__set_state(array(
   'container' => 
  array (
    'id' => '00U58000001IFRjEAO',
    'location' => 'To be determined',
    'subject' => 'United Oil & Gas, Singapore and MagneticOne',
    'description' => NULL,
    'status' => NULL,
    'result' => NULL,
    'startedAt' => NULL,
    'endedAt' => NULL,
    'parent' => NULL,
    'relation' => 
    Data2CRMAPI\Model\Relation::__set_state(array(
       'container' => 
      array (
        'account' => 
        array (
        ),
        'contact' => 
        array (
          0 => 
          Data2CRMAPI\Model\ContactRelation::__set_state(array(
             'container' => 
            array (
              'type' => 'Name ID (WhoId)',
              'entity' => 
              Data2CRMAPI\Model\ContactEntityRelation::__set_state(array(
                 'container' => 
                array (
                  'id' => '003580000024E1FAAU',
                ),
                 'raw' => 
                array (
                  'id' => '003580000024E1FAAU',
                ),
              )),
            ),
             'raw' => 
            array (
              'type' => 'Name ID (WhoId)',
              'entity' => 
              array (
                'id' => '003580000024E1FAAU',
              ),
            ),
          )),
        ),
        'lead' => 
        array (
        ),
        'opportunity' => 
        array (
        ),
        'task' => 
        array (
        ),
        'call' => 
        array (
        ),
        'email' => 
        array (
        ),
        'event' => 
        array (
        ),
        'meeting' => 
        array (
        ),
        'note' => 
        array (
        ),
        'attachment' => 
        array (
        ),
        'campaign' => 
        array (
        ),
        'project' => 
        array (
        ),
        'case' => 
        array (
        ),
      ),
       'raw' => 
      array (
        'contact' => 
        array (
          0 => 
          array (
            'type' => 'Name ID (WhoId)',
            'entity' => 
            array (
              'id' => '003580000024E1FAAU',
            ),
          ),
        ),
        'account' => 
        array (
        ),
        'lead' => 
        array (
        ),
        'opportunity' => 
        array (
        ),
        'task' => 
        array (
        ),
        'call' => 
        array (
        ),
        'email' => 
        array (
        ),
        'event' => 
        array (
        ),
        'meeting' => 
        array (
        ),
        'note' => 
        array (
        ),
        'attachment' => 
        array (
        ),
        'campaign' => 
        array (
        ),
        'project' => 
        array (
        ),
        'case' => 
        array (
        ),
      ),
    )),
    'assignedUser' => 
    Data2CRMAPI\Model\UserEntityRelation::__set_state(array(
       'container' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
       'raw' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
    )),
    'user' => 
    Data2CRMAPI\Model\UserEntityRelation::__set_state(array(
       'container' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
       'raw' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
    )),
    'createdBy' => 
    Data2CRMAPI\Model\UserEntityRelation::__set_state(array(
       'container' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
       'raw' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
    )),
    'createdAt' => 
    DateTime::__set_state(array(
       'date' => '2016-01-14 10:46:26',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
    'updatedBy' => 
    Data2CRMAPI\Model\UserEntityRelation::__set_state(array(
       'container' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
       'raw' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
    )),
    'updatedAt' => 
    DateTime::__set_state(array(
       'date' => '2016-01-14 10:46:28',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
   'raw' => 
  array (
    'id' => '00U58000001IFRjEAO',
    'location' => 'To be determined',
    'subject' => 'United Oil & Gas, Singapore and MagneticOne',
    'description' => NULL,
    'status' => NULL,
    'result' => NULL,
    'started_at' => NULL,
    'ended_at' => NULL,
    'parent' => NULL,
    'relation' => 
    array (
      'contact' => 
      array (
        0 => 
        array (
          'type' => 'Name ID (WhoId)',
          'entity' => 
          array (
            'id' => '003580000024E1FAAU',
          ),
        ),
      ),
      'account' => 
      array (
      ),
      'lead' => 
      array (
      ),
      'opportunity' => 
      array (
      ),
      'task' => 
      array (
      ),
      'call' => 
      array (
      ),
      'email' => 
      array (
      ),
      'event' => 
      array (
      ),
      'meeting' => 
      array (
      ),
      'note' => 
      array (
      ),
      'attachment' => 
      array (
      ),
      'campaign' => 
      array (
      ),
      'project' => 
      array (
      ),
      'case' => 
      array (
      ),
    ),
    'assigned_user' => 
    array (
      'id' => '00558000000dmueAAA',
    ),
    'user' => 
    array (
      'id' => '00558000000dmueAAA',
    ),
    'created_by' => 
    array (
      'id' => '00558000000dmueAAA',
    ),
    'created_at' => '2016-01-14T10:46:26+0000',
    'updated_by' => 
    array (
      'id' => '00558000000dmueAAA',
    ),
    'updated_at' => '2016-01-14T10:46:28+0000',
    'Custom_Auto_Number__c' => '22',
    'Custom_Checkbox__c' => false,
    'Custom_Currency__c' => NULL,
    'Custom_Date__c' => NULL,
    'Custom_Date_Time__c' => NULL,
    'Custom_Email__c' => NULL,
    'Custom_Number__c' => NULL,
    'Custom_Percent__c' => NULL,
    'Custom_Phone__c' => NULL,
    'Custom_Picklist__c' => NULL,
    'Custom_Text__c' => NULL,
    'Custom_Text_Area__c' => NULL,
    'Custom_Text_Encrypted__c' => NULL,
    'Custom_URL__c' => NULL,
  ),
))