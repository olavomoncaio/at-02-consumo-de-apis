Data2CRMAPI\Model\LeadEntity::__set_state(array(
   'container' => 
  array (
    'id' => '00Q58000001Ag02EAC',
    'salutation' => 'Ms',
    'firstName' => 'Patricia',
    'middleName' => NULL,
    'lastName' => 'Feager',
    'nameSuffix' => NULL,
    'description' => NULL,
    'position' => 'CEO',
    'department' => NULL,
    'company' => 'International Shipping Co.',
    'industry' => NULL,
    'annualRevenue' => NULL,
    'birthDate' => NULL,
    'type' => NULL,
    'rating' => NULL,
    'source' => 'Partner Referral',
    'sourceDescription' => NULL,
    'status' => 'Working - Contacted',
    'statusDescription' => NULL,
    'doNotCall' => NULL,
    'email' => 
    array (
      0 => 
      Data2CRMAPI\Model\Email::__set_state(array(
         'container' => 
        array (
          'type' => 'Email',
          'address' => 'patricia_feager@is.com',
        ),
         'raw' => 
        array (
          'type' => 'Email',
          'address' => 'patricia_feager@is.com',
        ),
      )),
    ),
    'phone' => 
    array (
      0 => 
      Data2CRMAPI\Model\Phone::__set_state(array(
         'container' => 
        array (
          'type' => 'Phone',
          'number' => '(336) 777-1970',
        ),
         'raw' => 
        array (
          'type' => 'Phone',
          'number' => '(336) 777-1970',
        ),
      )),
    ),
    'messenger' => 
    array (
    ),
    'website' => 
    array (
    ),
    'address' => 
    array (
      0 => 
      Data2CRMAPI\Model\Address::__set_state(array(
         'container' => 
        array (
          'type' => 'Address',
          'street' => NULL,
          'city' => NULL,
          'state' => 'NC',
          'country' => 'USA',
          'zip' => NULL,
        ),
         'raw' => 
        array (
          'type' => 'Address',
          'state' => 'NC',
          'country' => 'USA',
          'street' => NULL,
          'city' => NULL,
          'zip' => NULL,
        ),
      )),
    ),
    'parent' => NULL,
    'relation' => 
    Data2CRMAPI\Model\Relation::__set_state(array(
       'container' => 
      array (
        'account' => 
        array (
        ),
        'contact' => 
        array (
        ),
        'lead' => 
        array (
        ),
        'opportunity' => 
        array (
        ),
        'task' => 
        array (
        ),
        'call' => 
        array (
        ),
        'email' => 
        array (
        ),
        'event' => 
        array (
        ),
        'meeting' => 
        array (
        ),
        'note' => 
        array (
        ),
        'attachment' => 
        array (
        ),
        'campaign' => 
        array (
        ),
        'project' => 
        array (
        ),
        'case' => 
        array (
        ),
      ),
       'raw' => 
      array (
        'account' => 
        array (
        ),
        'contact' => 
        array (
        ),
        'lead' => 
        array (
        ),
        'opportunity' => 
        array (
        ),
        'task' => 
        array (
        ),
        'call' => 
        array (
        ),
        'email' => 
        array (
        ),
        'event' => 
        array (
        ),
        'meeting' => 
        array (
        ),
        'note' => 
        array (
        ),
        'attachment' => 
        array (
        ),
        'campaign' => 
        array (
        ),
        'project' => 
        array (
        ),
        'case' => 
        array (
        ),
      ),
    )),
    'assignedUser' => 
    Data2CRMAPI\Model\UserEntityRelation::__set_state(array(
       'container' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
       'raw' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
    )),
    'user' => 
    Data2CRMAPI\Model\UserEntityRelation::__set_state(array(
       'container' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
       'raw' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
    )),
    'createdBy' => 
    Data2CRMAPI\Model\UserEntityRelation::__set_state(array(
       'container' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
       'raw' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
    )),
    'createdAt' => 
    DateTime::__set_state(array(
       'date' => '2016-01-04 16:17:24',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
    'updatedBy' => 
    Data2CRMAPI\Model\UserEntityRelation::__set_state(array(
       'container' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
       'raw' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
    )),
    'updatedAt' => 
    DateTime::__set_state(array(
       'date' => '2016-01-04 16:17:24',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
   'raw' => 
  array (
    'id' => '00Q58000001Ag02EAC',
    'salutation' => 'Ms',
    'first_name' => 'Patricia',
    'middle_name' => NULL,
    'last_name' => 'Feager',
    'name_suffix' => NULL,
    'description' => NULL,
    'position' => 'CEO',
    'department' => NULL,
    'company' => 'International Shipping Co.',
    'industry' => NULL,
    'annual_revenue' => NULL,
    'birth_date' => NULL,
    'type' => NULL,
    'rating' => NULL,
    'source' => 'Partner Referral',
    'source_description' => NULL,
    'status' => 'Working - Contacted',
    'status_description' => NULL,
    'do_not_call' => NULL,
    'email' => 
    array (
      0 => 
      array (
        'type' => 'Email',
        'address' => 'patricia_feager@is.com',
      ),
    ),
    'phone' => 
    array (
      0 => 
      array (
        'type' => 'Phone',
        'number' => '(336) 777-1970',
      ),
    ),
    'messenger' => 
    array (
    ),
    'website' => 
    array (
    ),
    'address' => 
    array (
      0 => 
      array (
        'type' => 'Address',
        'state' => 'NC',
        'country' => 'USA',
        'street' => NULL,
        'city' => NULL,
        'zip' => NULL,
      ),
    ),
    'parent' => NULL,
    'relation' => 
    array (
      'account' => 
      array (
      ),
      'contact' => 
      array (
      ),
      'lead' => 
      array (
      ),
      'opportunity' => 
      array (
      ),
      'task' => 
      array (
      ),
      'call' => 
      array (
      ),
      'email' => 
      array (
      ),
      'event' => 
      array (
      ),
      'meeting' => 
      array (
      ),
      'note' => 
      array (
      ),
      'attachment' => 
      array (
      ),
      'campaign' => 
      array (
      ),
      'project' => 
      array (
      ),
      'case' => 
      array (
      ),
    ),
    'assigned_user' => 
    array (
      'id' => '00558000000dmueAAA',
    ),
    'user' => 
    array (
      'id' => '00558000000dmueAAA',
    ),
    'created_by' => 
    array (
      'id' => '00558000000dmueAAA',
    ),
    'created_at' => '2016-01-04T16:17:24+0000',
    'updated_by' => 
    array (
      'id' => '00558000000dmueAAA',
    ),
    'updated_at' => '2016-01-04T16:17:24+0000',
    'SICCode__c' => '2768',
    'ProductInterest__c' => 'GC5000 series',
    'Primary__c' => 'Yes',
    'CurrentGenerators__c' => 'All',
    'NumberofLocations__c' => 130,
    'Customfield1__c' => NULL,
    'Custom_Auto_Number__c' => NULL,
    'Custom_Formula__c' => NULL,
    'Custom_Checkbox__c' => false,
    'Custom_Currency__c' => NULL,
    'Custom_Date__c' => NULL,
    'Custom_Date_Time__c' => NULL,
    'Custom_Number__c' => NULL,
    'Custom_Percent__c' => NULL,
    'Custom_Picklist__c' => NULL,
    'Custom_Text__c' => NULL,
    'Custom_Text_Area__c' => NULL,
    'Custom_Text_Area_Long__c' => NULL,
    'Custom_Text_Area_Rich__c' => NULL,
    'Custom_Text_Encrypted__c' => NULL,
  ),
))