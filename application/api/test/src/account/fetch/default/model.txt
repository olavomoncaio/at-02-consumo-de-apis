Data2CRMAPI\Model\AccountEntity::__set_state(array(
   'container' => 
  array (
    'id' => '00158000002bRg3AAE',
    'type' => 'Customer - Direct',
    'name' => 'Edge Comunic',
    'tickerSymbol' => 'EDGE',
    'employees' => '12',
    'ownership' => 'Public',
    'industry' => 'Electronics',
    'description' => 'Edge, founded in 1998, is a start-up based in Austin, TX. The company designs and manufactures a device to convert music from one digital format to another. Edge sells its product through retailers and its own website.',
    'annualRevenue' => '139000000',
    'sicCode' => '6576',
    'rating' => 'Hot',
    'category' => NULL,
    'email' => 
    array (
    ),
    'phone' => 
    array (
      0 => 
      Data2CRMAPI\Model\Phone::__set_state(array(
         'container' => 
        array (
          'type' => 'Account Phone',
          'number' => '(512) 757-6000',
        ),
         'raw' => 
        array (
          'type' => 'Account Phone',
          'number' => '(512) 757-6000',
        ),
      )),
      1 => 
      Data2CRMAPI\Model\Phone::__set_state(array(
         'container' => 
        array (
          'type' => 'Account Fax',
          'number' => '(512) 757-9000',
        ),
         'raw' => 
        array (
          'type' => 'Account Fax',
          'number' => '(512) 757-9000',
        ),
      )),
    ),
    'messenger' => 
    array (
    ),
    'website' => 
    array (
      0 => 
      Data2CRMAPI\Model\Website::__set_state(array(
         'container' => 
        array (
          'type' => 'Website',
          'address' => 'http://edgecomm.com',
        ),
         'raw' => 
        array (
          'type' => 'Website',
          'address' => 'http://edgecomm.com',
        ),
      )),
    ),
    'address' => 
    array (
      0 => 
      Data2CRMAPI\Model\Address::__set_state(array(
         'container' => 
        array (
          'type' => 'Billing',
          'street' => '312 Constitution Place
Austin, TX 78767
USA',
          'city' => 'Austin',
          'state' => 'TX',
          'country' => NULL,
          'zip' => NULL,
        ),
         'raw' => 
        array (
          'type' => 'Billing',
          'street' => '312 Constitution Place
Austin, TX 78767
USA',
          'city' => 'Austin',
          'state' => 'TX',
          'country' => NULL,
          'zip' => NULL,
        ),
      )),
      1 => 
      Data2CRMAPI\Model\Address::__set_state(array(
         'container' => 
        array (
          'type' => 'Shipping',
          'street' => '312 Constitution Place
Austin, TX 78767
USA',
          'city' => NULL,
          'state' => NULL,
          'country' => NULL,
          'zip' => NULL,
        ),
         'raw' => 
        array (
          'type' => 'Shipping',
          'street' => '312 Constitution Place
Austin, TX 78767
USA',
          'city' => NULL,
          'state' => NULL,
          'country' => NULL,
          'zip' => NULL,
        ),
      )),
    ),
    'parent' => NULL,
    'relation' => 
    Data2CRMAPI\Model\Relation::__set_state(array(
       'container' => 
      array (
        'account' => 
        array (
          0 => 
          Data2CRMAPI\Model\AccountRelation::__set_state(array(
             'container' => 
            array (
              'type' => 'Account3333 (Accou1111111nt3333__c)',
              'entity' => 
              Data2CRMAPI\Model\AccountEntityRelation::__set_state(array(
                 'container' => 
                array (
                  'id' => '00158000002bRg4AAE',
                ),
                 'raw' => 
                array (
                  'id' => '00158000002bRg4AAE',
                ),
              )),
            ),
             'raw' => 
            array (
              'type' => 'Account3333 (Accou1111111nt3333__c)',
              'entity' => 
              array (
                'id' => '00158000002bRg4AAE',
              ),
            ),
          )),
          1 => 
          Data2CRMAPI\Model\AccountRelation::__set_state(array(
             'container' => 
            array (
              'type' => 'Account3333 (Account3333__c)',
              'entity' => 
              Data2CRMAPI\Model\AccountEntityRelation::__set_state(array(
                 'container' => 
                array (
                  'id' => '001580000080sz4AAA',
                ),
                 'raw' => 
                array (
                  'id' => '001580000080sz4AAA',
                ),
              )),
            ),
             'raw' => 
            array (
              'type' => 'Account3333 (Account3333__c)',
              'entity' => 
              array (
                'id' => '001580000080sz4AAA',
              ),
            ),
          )),
        ),
        'contact' => 
        array (
        ),
        'lead' => 
        array (
        ),
        'opportunity' => 
        array (
        ),
        'task' => 
        array (
        ),
        'call' => 
        array (
        ),
        'email' => 
        array (
        ),
        'event' => 
        array (
        ),
        'meeting' => 
        array (
        ),
        'note' => 
        array (
        ),
        'attachment' => 
        array (
        ),
        'campaign' => 
        array (
          0 => 
          Data2CRMAPI\Model\CampaignRelation::__set_state(array(
             'container' => 
            array (
              'type' => 'Campaignsssssssss (ddddddddddddd__c)',
              'entity' => 
              Data2CRMAPI\Model\CampaignEntityRelation::__set_state(array(
                 'container' => 
                array (
                  'id' => '70158000000HmrlAAC',
                ),
                 'raw' => 
                array (
                  'id' => '70158000000HmrlAAC',
                ),
              )),
            ),
             'raw' => 
            array (
              'type' => 'Campaignsssssssss (ddddddddddddd__c)',
              'entity' => 
              array (
                'id' => '70158000000HmrlAAC',
              ),
            ),
          )),
        ),
        'project' => 
        array (
        ),
        'case' => 
        array (
          0 => 
          Data2CRMAPI\Model\CaseRelation::__set_state(array(
             'container' => 
            array (
              'type' => 'Case11111 (Case11111111111111__c)',
              'entity' => 
              Data2CRMAPI\Model\CaseEntityRelation::__set_state(array(
                 'container' => 
                array (
                  'id' => '50058000001WbI0AAK',
                ),
                 'raw' => 
                array (
                  'id' => '50058000001WbI0AAK',
                ),
              )),
            ),
             'raw' => 
            array (
              'type' => 'Case11111 (Case11111111111111__c)',
              'entity' => 
              array (
                'id' => '50058000001WbI0AAK',
              ),
            ),
          )),
        ),
      ),
       'raw' => 
      array (
        'account' => 
        array (
          0 => 
          array (
            'type' => 'Account3333 (Accou1111111nt3333__c)',
            'entity' => 
            array (
              'id' => '00158000002bRg4AAE',
            ),
          ),
          1 => 
          array (
            'type' => 'Account3333 (Account3333__c)',
            'entity' => 
            array (
              'id' => '001580000080sz4AAA',
            ),
          ),
        ),
        'campaign' => 
        array (
          0 => 
          array (
            'type' => 'Campaignsssssssss (ddddddddddddd__c)',
            'entity' => 
            array (
              'id' => '70158000000HmrlAAC',
            ),
          ),
        ),
        'case' => 
        array (
          0 => 
          array (
            'type' => 'Case11111 (Case11111111111111__c)',
            'entity' => 
            array (
              'id' => '50058000001WbI0AAK',
            ),
          ),
        ),
        'contact' => 
        array (
        ),
        'lead' => 
        array (
        ),
        'opportunity' => 
        array (
        ),
        'task' => 
        array (
        ),
        'call' => 
        array (
        ),
        'email' => 
        array (
        ),
        'event' => 
        array (
        ),
        'meeting' => 
        array (
        ),
        'note' => 
        array (
        ),
        'attachment' => 
        array (
        ),
        'project' => 
        array (
        ),
      ),
    )),
    'assignedUser' => 
    Data2CRMAPI\Model\UserEntityRelation::__set_state(array(
       'container' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
       'raw' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
    )),
    'user' => 
    Data2CRMAPI\Model\UserEntityRelation::__set_state(array(
       'container' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
       'raw' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
    )),
    'createdBy' => 
    Data2CRMAPI\Model\UserEntityRelation::__set_state(array(
       'container' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
       'raw' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
    )),
    'createdAt' => 
    DateTime::__set_state(array(
       'date' => '2016-01-04 16:17:24',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
    'updatedBy' => 
    Data2CRMAPI\Model\UserEntityRelation::__set_state(array(
       'container' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
       'raw' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
    )),
    'updatedAt' => 
    DateTime::__set_state(array(
       'date' => '2016-06-21 14:44:42',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
   'raw' => 
  array (
    'id' => '00158000002bRg3AAE',
    'type' => 'Customer - Direct',
    'name' => 'Edge Comunic',
    'ticker_symbol' => 'EDGE',
    'employees' => '12',
    'ownership' => 'Public',
    'industry' => 'Electronics',
    'description' => 'Edge, founded in 1998, is a start-up based in Austin, TX. The company designs and manufactures a device to convert music from one digital format to another. Edge sells its product through retailers and its own website.',
    'annual_revenue' => 139000000,
    'sic_code' => '6576',
    'rating' => 'Hot',
    'category' => NULL,
    'email' => 
    array (
    ),
    'phone' => 
    array (
      0 => 
      array (
        'type' => 'Account Phone',
        'number' => '(512) 757-6000',
      ),
      1 => 
      array (
        'type' => 'Account Fax',
        'number' => '(512) 757-9000',
      ),
    ),
    'messenger' => 
    array (
    ),
    'website' => 
    array (
      0 => 
      array (
        'type' => 'Website',
        'address' => 'http://edgecomm.com',
      ),
    ),
    'address' => 
    array (
      0 => 
      array (
        'type' => 'Billing',
        'street' => '312 Constitution Place
Austin, TX 78767
USA',
        'city' => 'Austin',
        'state' => 'TX',
        'country' => NULL,
        'zip' => NULL,
      ),
      1 => 
      array (
        'type' => 'Shipping',
        'street' => '312 Constitution Place
Austin, TX 78767
USA',
        'city' => NULL,
        'state' => NULL,
        'country' => NULL,
        'zip' => NULL,
      ),
    ),
    'parent' => NULL,
    'relation' => 
    array (
      'account' => 
      array (
        0 => 
        array (
          'type' => 'Account3333 (Accou1111111nt3333__c)',
          'entity' => 
          array (
            'id' => '00158000002bRg4AAE',
          ),
        ),
        1 => 
        array (
          'type' => 'Account3333 (Account3333__c)',
          'entity' => 
          array (
            'id' => '001580000080sz4AAA',
          ),
        ),
      ),
      'campaign' => 
      array (
        0 => 
        array (
          'type' => 'Campaignsssssssss (ddddddddddddd__c)',
          'entity' => 
          array (
            'id' => '70158000000HmrlAAC',
          ),
        ),
      ),
      'case' => 
      array (
        0 => 
        array (
          'type' => 'Case11111 (Case11111111111111__c)',
          'entity' => 
          array (
            'id' => '50058000001WbI0AAK',
          ),
        ),
      ),
      'contact' => 
      array (
      ),
      'lead' => 
      array (
      ),
      'opportunity' => 
      array (
      ),
      'task' => 
      array (
      ),
      'call' => 
      array (
      ),
      'email' => 
      array (
      ),
      'event' => 
      array (
      ),
      'meeting' => 
      array (
      ),
      'note' => 
      array (
      ),
      'attachment' => 
      array (
      ),
      'project' => 
      array (
      ),
    ),
    'assigned_user' => 
    array (
      'id' => '00558000000dmueAAA',
    ),
    'user' => 
    array (
      'id' => '00558000000dmueAAA',
    ),
    'created_by' => 
    array (
      'id' => '00558000000dmueAAA',
    ),
    'created_at' => '2016-01-04T16:17:24+0000',
    'updated_by' => 
    array (
      'id' => '00558000000dmueAAA',
    ),
    'updated_at' => '2016-06-21T14:44:42+0000',
    'SLA__c' => 'Silver',
    'NumberofLocations__c' => 211,
    'UpsellOpportunity__c' => 'Maybe',
    'SLASerialNumber__c' => '2657',
    'SLAExpirationDate__c' => '2015-01-29',
    'myfir__c' => 999,
    'CustomerPriority__c' => NULL,
    'TEST_Field__c' => NULL,
    'Custom_Auto_Number__c' => NULL,
    'Custom_Formula__c' => 223,
    'Roll_Up_Summary__c' => 220000,
    'Custom_Checkbox__c' => false,
    'Custom_Currency__c' => NULL,
    'Custom_Date__c' => NULL,
    'Custom_DateTime__c' => NULL,
    'Custom_Number__c' => NULL,
    'Custom_Percent__c' => NULL,
    'Custom_Picklist__c' => NULL,
    'Custom_Text__c' => NULL,
    'Custom_Textarea__c' => NULL,
    'Custom_Text_Area_Long__c' => NULL,
    'Text_Area_Rich__c' => NULL,
    'Custom_Text_Encrypted__c' => NULL,
    'Custy__c' => '11',
    'CustDate__c' => '2016-06-03',
    'CustCheck111111111111111111111__c' => '2016-06-09T14:43:00+0000',
    'CustChec__c' => NULL,
  ),
))