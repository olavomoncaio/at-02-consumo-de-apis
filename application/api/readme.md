[All SDKs](/sdk)
<div style="position: relative;">
# PHP SDK

   * [Requirements](#requirements)
   * [Installation](#installation)
   * [Getting Started](#getting-started)
   * [How to Use](#how-to-use)
       * [Create application](#create-application)
       * [Describe a resource](#describe-a-resource)
       * [Count all of resources](#count-all-of-resources)
       * [Fetch all of resources](#fetch-all-of-resources)
       * [Fetch all of resources (with filter)](#fetch-all-of-resources-with-filter)
       * [Fetch a resource](#fetch-a-resource)
       * [Create a resource](#create-a-resource)
       * [Update a resource](#update-a-resource)
       * [Delete a resource](#delete-a-resource)

## Requirements

   * PHP 5.3.3 and later
   * PHP Extension CURL
   * PHP Extension JSON
   * PHP Extension mbstring

## <a name="installation"></a>Installation

Install the latest version with the following [Composer](http://getcomposer.org/) command:

    $ composer require "data2crm-api/php-sdk:dev-master" 

Alternately, manually add the following to your `composer.json`, in the `require` section:

    "require": {
        "data2crm-api/php-sdk": "dev-master"
    }

## <a name="getting-started"></a>Getting Started
    
    // Create configuration
    $configuration = new \Data2CRMAPI\Configuration();
    $configuration->setUserKey('<USER_KEY>'); // change <USER_KEY> to your user key
    $configuration->setApplicationKey('<APPLICATION_KEY>'); // change <APPLICATION_KEY> to your application key
    
    // Create client
    $client = new \Data2CRMAPI\ApiClient($configuration);

## <a name="how-to-use"></a>How to Use

### <a name="create-application"></a>Create application
    
    // Create application API
    $api = new \Data2CRMAPI\Resource\ApplicationApi($client);
    
    try {
    
        $apiToken = new \Data2CRMAPI\Model\Credential();
        $apiToken->setName('api_token');
        $apiToken->setValue('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
        
        $application = new \Data2CRMAPI\Model\ApplicationEntityWrite();
        $application->setType('Pipedrive');
        $application->setAuthorization('key');
        $application->setDescription('SDK Test');
        $application->setCredential(
            [
                $apiToken,
            ]
        );
        
        $applicationRelation = $api->create($application);
    
        echo 'Application #' . $applicationRelation->getKey() . PHP_EOL;
    
    } catch (\Data2CRMAPI\ApiException $exception) {
        echo 'Failed: #' 
            . $exception->getCode() 
            . ' => ' . $exception->getDetailCode() . ': ' . $exception->getDetailMessage()
            . ' => ' . print_r($exception->getDetailExtra(), true) . PHP_EOL
            . PHP_EOL;
    }

### <a name="describe-a-resource"></a>Describe a resource
    
    // Create contact API
    $api = new \Data2CRMAPI\Resource\ContactApi($client);
    
    try {
    
        $describe = $api->describe();
    
        print_r($describe->getSchema());
    
    } catch (\Data2CRMAPI\ApiException $exception) {
        echo 'Failed: #' 
            . $exception->getCode() 
            . ' => ' . $exception->getDetailCode() . ': ' . $exception->getDetailMessage()
            . ' => ' . print_r($exception->getDetailExtra(), true) . PHP_EOL
            . PHP_EOL;
    }
    
### <a name="count-all-of-resources"></a>Count all of resources
    
    // Create contact API
    $api = new \Data2CRMAPI\Resource\ContactApi($client);
    
    try {
    
        $count = $api->count();
    
        echo 'Contacts count: ' . $count->getTotal() . PHP_EOL;
    
    } catch (\Data2CRMAPI\ApiException $exception) {
        echo 'Failed: #' 
            . $exception->getCode() 
            . ' => ' . $exception->getDetailCode() . ': ' . $exception->getDetailMessage()
            . ' => ' . print_r($exception->getDetailExtra(), true) . PHP_EOL
            . PHP_EOL;
    }

### <a name="fetch-all-of-resources"></a>Fetch all of resources
    
    // Create contact API
    $api = new \Data2CRMAPI\Resource\ContactApi($client);
    
    try {
    
        $contacts = $api->fetchAll();
    
        foreach ($contacts as $contact) {
            echo '=== Contact #' . $contact->getId() . ' ===' . PHP_EOL;
            echo 'First Name: ' . $contact->getFirstName() . PHP_EOL;
            echo 'Last Name: ' . $contact->getLastName() . PHP_EOL;
            foreach ($contact->getEmail() as $email) {
                echo 'Email ' . $email->getType() . ': ' . $email->getAddress() . PHP_EOL;
            }
            echo PHP_EOL;
        }
    
    } catch (\Data2CRMAPI\ApiException $exception) {
        echo 'Failed: #' 
            . $exception->getCode() 
            . ' => ' . $exception->getDetailCode() . ': ' . $exception->getDetailMessage()
            . ' => ' . print_r($exception->getDetailExtra(), true) . PHP_EOL
            . PHP_EOL;
    }

### <a name="fetch-all-of-resources-with-filter"></a>Fetch all of resources with filter

    // Create contact API
    $api = new \Data2CRMAPI\Resource\ContactApi($client);
    
    try {
    
        $contacts = $api->fetchAll(25, 1, ['first_name' => 'Demo']);
        
        if (true === $api->getData()->isStatusDone()) {
            foreach ($contacts as $contact) {
                echo '=== Contact #' . $contact->getId() . ' ===' . PHP_EOL;
                echo 'First Name: ' . $contact->getFirstName() . PHP_EOL;
                echo 'Last Name: ' . $contact->getLastName() . PHP_EOL;
                foreach ($contact->getEmail() as $email) {
                    echo 'Email ' . $email->getType() . ': ' . $email->getAddress() . PHP_EOL;
                }
                echo PHP_EOL;
            }
        } else {
            echo 'Data status message: ' . $api->getData()->getStatusMessage() . PHP_EOL;
        }
    
    } catch (\Data2CRMAPI\ApiException $exception) {
        echo 'Failed: #' 
            . $exception->getCode() 
            . ' => ' . $exception->getDetailCode() . ': ' . $exception->getDetailMessage()
            . ' => ' . print_r($exception->getDetailExtra(), true) . PHP_EOL
            . PHP_EOL;
    }

### <a name="fetch-a-resource"></a>Fetch a resource

    // Create contact API
    $api = new \Data2CRMAPI\Resource\ContactApi($client);
    
    try {
    
        $contact = $api->fetch('12345');
    
        echo '=== Contact #' . $contact->getId() . ' ===' . PHP_EOL;
        echo 'First Name: ' . $contact->getFirstName() . PHP_EOL;
        echo 'Last Name: ' . $contact->getLastName() . PHP_EOL;
        foreach ($contact->getEmail() as $email) {
            echo 'Email ' . $email->getType() . ': ' . $email->getAddress() . PHP_EOL;
        }
        echo PHP_EOL;
    
    } catch (\Data2CRMAPI\ApiException $exception) {
        echo 'Failed: #' 
            . $exception->getCode() 
            . ' => ' . $exception->getDetailCode() . ': ' . $exception->getDetailMessage()
            . ' => ' . print_r($exception->getDetailExtra(), true) . PHP_EOL
            . PHP_EOL;
    }

### <a name="create-a-resource"></a>Create a resource

    // Create contact API
    $api = new \Data2CRMAPI\Resource\ContactApi($client);
    
    try {
    
        $email = new \Data2CRMAPI\Model\Email();
        $email->setType('Home');
        $email->setAddress('some.email@mail.com');
    
        $contact = new \Data2CRMAPI\Model\ContactEntity();
        $contact->setFirstName('Some First Name');
        $contact->setLastName('Some Last Name');
        $contact->setEmail(
            [
                $email
            ]
        );
        
        $contactEntityRelation = $api->create($contact);
    
        echo 'Contact #' . $contactEntityRelation->getId();
    
    } catch (\Data2CRMAPI\ApiException $exception) {
        echo 'Failed: #' 
            . $exception->getCode() 
            . ' => ' . $exception->getDetailCode() . ': ' . $exception->getDetailMessage()
            . ' => ' . print_r($exception->getDetailExtra(), true) . PHP_EOL
            . PHP_EOL;
    }

### <a name="update-a-resource"></a>Update a resource

    // Create contact API
    $api = new \Data2CRMAPI\Resource\ContactApi($client);
    
    try {
    
        $email = new \Data2CRMAPI\Model\Email();
        $email->setType('Home');
        $email->setAddress('some.email@mail.com');
    
        $contact = new \Data2CRMAPI\Model\ContactEntity();
        $contact->setFirstName('Some First Name');
        $contact->setLastName('Some Last Name');
        $contact->setEmail(
            [
                $email
            ]
        );
        
        $contactEntityRelation = $api->update('12345', $contact);
    
        echo 'Contact #' . $contactEntityRelation->getId();
    
    } catch (\Data2CRMAPI\ApiException $exception) {
        echo 'Failed: #' 
            . $exception->getCode() 
            . ' => ' . $exception->getDetailCode() . ': ' . $exception->getDetailMessage()
            . ' => ' . print_r($exception->getDetailExtra(), true) . PHP_EOL
            . PHP_EOL;
    }

### <a name="delete-a-resource"></a>Delete a resource

    // Create contact API
    $api = new \Data2CRMAPI\Resource\ContactApi($client);
    
    try {
    
        $api->delete('12345');
    
    } catch (\Data2CRMAPI\ApiException $exception) {
        echo 'Failed: #' 
            . $exception->getCode() 
            . ' => ' . $exception->getDetailCode() . ': ' . $exception->getDetailMessage()
            . ' => ' . print_r($exception->getDetailExtra(), true) . PHP_EOL
            . PHP_EOL;
    }
    
</div>
