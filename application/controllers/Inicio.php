<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

	public function index()
	{
                $this->load->view('common/header');  
                $this->load->view('common/navbar');
                $this->load->view('inicio/start');
                $this->load->view('common/footer');
        }

        public function api(){
                $this->load->view('common/header');  
                $this->load->view('common/navbar');
                $this->load->view('inicio/api');
                $this->load->view('common/footer');
        }

        public function funcionamento(){
                $this->load->view('common/header');  
                $this->load->view('common/navbar');
                $this->load->view('inicio/funcionamento');
                $this->load->view('common/footer');
        }

        
        public function interacao(){
                $this->load->view('common/header');  
                $this->load->view('common/navbar');
                $this->load->view('inicio/interacao');
                $this->load->view('common/footer');
        }
}
